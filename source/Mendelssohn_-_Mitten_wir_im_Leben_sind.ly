\version "2.24.3"
\language "deutsch"

#(set-default-paper-size "a4")
#(set-global-staff-size 15)

\paper {
  % annotate-spacing = ##t 
  % indent = 0\cm
}

\header {
  title = "Mendelssohn – Mitten wir im Leben sind"
  pdftitle = "Mendelssohn – Mitten wir im Leben sind"
  subject = "unterwegs behütet"
  author= "Richard Hirsch"
  copyright = "Konkordien-Kantorei Mannheim 2024 (rev. 1) – Vervielfältigung und Verbreitung erlaubt"
  tagline = ""
}

mitten_wir_meta = {
  \key c \minor
  \time 2/2
  \dynamicUp
  \autoBeamOff
}

\include "Mendelssohn - Mitten wir im Leben sind/soprano1.ly"
\include "Mendelssohn - Mitten wir im Leben sind/soprano2.ly"
\include "Mendelssohn - Mitten wir im Leben sind/alto1.ly"
\include "Mendelssohn - Mitten wir im Leben sind/alto2.ly"
\include "Mendelssohn - Mitten wir im Leben sind/tenore1.ly"
\include "Mendelssohn - Mitten wir im Leben sind/tenore2.ly"
\include "Mendelssohn - Mitten wir im Leben sind/basso1.ly"
\include "Mendelssohn - Mitten wir im Leben sind/basso2.ly"

mitten_wir_one_text_f = \lyricmode {
  Das bist du, Herr, al -- lei -- ne.
}

mitten_wir_two_text_f = \lyricmode {
  Das tust du, Herr, al -- lei -- ne.
}

mitten_wir_one_text_m = \lyricmode {
  Mit -- ten wir im Le -- ben sind mit dem Tod um -- fan -- gen.
  Wen such'n wir, der Hil -- fe tu, daß wir Gnad er -- lan -- gen?
  Uns reu -- et uns -- re Mis -- se -- tat,
  die dich,  Herr, er -- zür -- net hat.
}

mitten_wir_two_text_m = \lyricmode {
  Mit -- ten in dem Tod an -- ficht uns der Höl -- len Ra -- chen.
  Wer will uns aus sol -- cher Not frei und le -- dig ma -- chen?
  Es jam -- mert dein Barm -- her -- zig -- keit
  uns -- re Sünd und gro -- ßes Leid.
}

mitten_wir_three_text_f = \lyricmode {
  Mit -- ten in der Höl -- len Angst un -- re Sünd uns trei -- ben.
  Wo solln wir denn flie -- hen hin, da wir mö -- gen __ blei -- ben?
  Zu dir, Herr Christ,  al -- lei -- ne. 
  Ver -- gos -- sen ist dein teu -- res Blut,
  das gnug für die Sün -- de tut.
}

mitten_wir_three_text_m = \lyricmode {
  Mit -- ten in der Höl -- len Angst un -- re Sünd uns trei -- ben.
  Wo solln wir denn flie -- hen hin, da wir mö -- gen __ blei -- ben?
  Zu dir al -- lei -- ne.
  Ver -- gos -- sen ist dein teu -- res Blut,
  das gnug für die Sün -- de tut.
}


mitten_wir_text_h = \lyricmode {
  Hei -- li -- ger Her -- re Gott,  hei -- li -- ger star -- ker Gott, __
  Hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
}



%%% Stanza 1
\score {
  \header { piece = "Mitten wir im Leben sind – Strophe 1" }
  <<
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano1-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto1-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore1-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso1-1.ly"
    >>
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano2-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto2-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore2-1.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso2-1.ly"
    >>
  >>
}

%%% Stanza 2
\score {
  \header { piece = "Mitten wir im Leben sind – Strophe 2" }
  <<
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano1-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto1-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore1-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso1-2.ly"
    >>
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano2-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto2-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore2-2.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso2-2.ly"
    >>
  >>
}

%%% Stanza 3
\score {
  \header { piece = "Mitten wir im Leben sind – Strophe 3" }
  <<
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano1-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto1-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore1-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso1-3.ly"
    >>
    \new ChoirStaff <<
      \include "Mendelssohn - Mitten wir im Leben sind/soprano2-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/alto2-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/tenore2-3.ly"
      \include "Mendelssohn - Mitten wir im Leben sind/basso2-3.ly"
    >>
  >>
}
