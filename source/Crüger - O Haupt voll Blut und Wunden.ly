\version "2.24.3"
\language "deutsch"

#(set-default-paper-size "a4")

\paper {
%  indent = 0\cm
}

\header {
  title = "O Haupt voll Blut und Wunden"
  subtitle = "EG 85"
  composer = "Johann Crüger, 1649"
  poet = "Paul Gerhardt, 1656"
  tagline = "Konkordien-Kantorei Mannheim 2024 (rev. 1) – Vervielfältigung und Verbreitung erlaubt"
}

global = {
  \override Score.BarLine.stencil = ##f
  \key c \major
  \time 4/4
}

stanzaii = \lyricmode {
  Du e -- dles An -- ge -- si -- chte, da -- vor sonst schrickt und scheut
  das gro -- ße Welt ge -- wi  -- chte: wie bist du so
  be -- speit, wie   bist du so er -- blei -- chet!
  Wer hat dein Au -- gen -- licht, dem sonst kein Licht nicht glei -- chet, 
  so schänd -- lich 
}

stanzavi = \lyricmode {
  Ich will hier bei dir ste -- hen, ver -- acht -- te mich doch nicht;
  von dir will ich nicht ge -- hen, wenn dir dein Her -- ze bricht; 
  wenn dein Haupt wird er -- blas -- sen im letz -ten To -- des -- stoß,
  als dann will ich dich fas -- sen in mei -- nem Arm und }

\score {
  \new ChoirStaff <<
    \new Staff = "soprano" <<
      \new Voice = "soprano" { 
        \global
	\relative c' { e2 a4 g  f e d2  e2 r4 h'4  c4 c h \revert Score.BarLine.stencil h a2
		       \override Score.BarLine.stencil = ##f
		       e2 a4 g  f e d2  e2 r4 h'4  c4 c h h \revert Score.BarLine.stencil a2 
		       c2 \override Score.BarLine.stencil = ##f h4 g a h c2 c4 g a g f f e2
		       c'? h4 \revert Score.BarLine.stencil d c
		       \override Score.BarLine.stencil = ##f
		       h a2 h4 e, f e d d e1~ e1
		       \fine
		     }
      }
      \new Lyrics = "soprano2" {
	\lyricsto "soprano" {
	  \set stanza = "2. "
	  \stanzaii
	  zu -- ge -- richt? __
	}
      }
      \new Lyrics = "soprano6" {
	\lyricsto "soprano" {
	  \set stanza = "6. "
	  \stanzavi
	  Schoß. __
	}
      }
    >>
    \new Staff = "alto" <<
      \new Voice = "alto" {
        \global
	\relative c' { \repeat unfold 2 {g2 a4 c a8( \noBeam h)  c4. ( h16 a) h4 c2  
					 r4 d e4 e e e cis2 }
		       e2 g4 c, c d e2 e4 e f e d d cis2
		       r4 e g g e d d2 d4
		       c4 d c c h c\fermata c a h c c h2
		       \revert Score.BarLine.stencil \fine
		     }
      }
      \new Lyrics {
	\lyricsto "alto" {
	  \set stanza = "2. "
	  \stanzaii
	  zu -- ge -- richt, so schänd -- lich zu -- ge -- richt?
	}
      }
      \new Lyrics {
	\lyricsto "alto" {
	  \set stanza = "6. "
	  \stanzavi
	  Schoß, in mei -- nem Arm und Schoß.
	}
      }
    >>
    \new Staff = "tenor" <<
      \new Voice = "tenor" {
	\clef "violin_8"
        \global
	\relative c' { \repeat unfold 2 { c2 c4 c d g, g2 g r4 g g4 a a gis a2 }
		       r4 a4 d4 c a f g2 g4 c c c a a a2
		       r4 a4 g g g g~ g fis g 
		       g b g g g g \fermata
		       a c h? a a gis2
		       \revert Score.BarLine.stencil \fine
		     }
      }
      \new Lyrics {
	\lyricsto "tenor" {
	  \set stanza = "2. "
	  \stanzaii
	  zu -- ge -- richt, so schänd -- lich zu -- ge -- richt?
	}
      }
      \new Lyrics {
	\lyricsto "tenor" {
	  \set stanza = "6. "
	  \stanzavi
	  Schoß, in mei -- nem Arm und Schoß.
	}
      }
    >>
    \new Staff = "bass" <<
      \new Voice = "bass" {
	\clef "bass"
        \global
	\relative c { \repeat unfold 2 { c2 f4 e d c g2 c  r4 g c4 a e' e a,2 }
		      a'2 g4 e f d c2 c4 c f c d d a2 a 
		      e'4 h c g d'2 g,4 c b c g g c\fermata a a gis a a e2
		      \revert Score.BarLine.stencil \fine
		    }
      }
      \new Lyrics = "bass" {
	\lyricsto "bass" {
	  \set stanza = "2. "
	  \stanzaii
	  zu -- ge -- richt, so schänd -- lich zu -- ge -- richt?
	}
      }
      \new Lyrics = "bass" {
	\lyricsto "bass" {
	  \set stanza = "6. "
	  \stanzavi
	  Schoß, in mei -- nem Arm und Schoß.
	}
      }
    >>
  >>
}