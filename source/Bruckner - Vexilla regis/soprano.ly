vexilla_one_soprano = \relative c' {
          e1^\p^\markup { \large \bold { Molto sostenuto } } | 
          e2 \< f | 
          f2 ( \! e2~ | 
          e4 d ) \> g ( f ) \! |
          e2 ( c4 d | 
          e f e8 g a h |
%%%       \set crescendoText = \markup { \italic { cresc. sempre } }
%%%       \set crescendoSpanner = #'text
%%%       \override DynamicTextSpanner.style = #'dotted-line
          c4^\markup { \italic { cresc. sempre } } a8 h c4 ) d |
          d2  r2

          \barNumberCheck 9
          dis2^\f fis |
          cis dis |
          e ( d! | c!2. ) c4 |
          h2^\markup { \italic { dim. sempre } } e,4 e |
          fis2. fis4 |
          gis2 r2 |
          r2 a^\p |
          a1 \< ( |
          b2-> \! ) a |

          \barNumberCheck 19
          d1-> ( |
          d2 ) \> c |
          c1^\p~ ( |
          c4 b ) a ( b ) |
          a2 r2 |
          r2 a2 |
          h!1\< \after 8 \! c2 h2
          b1~ | b2 b \> |

          \barNumberCheck 29
          as2 ( g ) | 
          r2 \! g4( f ) |
          e!1~ |
          e1~ \< |
          e1~ \! |
          e2. e4 \> \after 4 \! 
          e1 \bar "||" \barFermata
        }


vexilla_two_soprano = {
  \set Score.currentBarNumber = 36
  \relative c' {
    \barNumberCheck 36
    e1~ ^\p \< |
    e2  \! f ( |
    f2 ) e (  |
    e4 \> d4 ) g ( f ) \! 
    e2 c4 ( d |
    e f e8^\crescsempre g a h c4  a8 h c4 ) d |
    d2 r2 |

    \barNumberCheck 44
    dis2^\f fis4. fis8 |
    cis2 dis |
    e ( d!  |
    c!2. ) c4 |
    h2^\dimsempre e, |
    fis2. fis4 |
    gis2 r2 |

    \barNumberCheck 51
    r2 a2~^\p ( |
    a1 \< \after 4 \! 
    b2 ) a 
    d1 |
    d2 \>  c
    \after 2 \! c1~ ( 
    c4 b4 )  a ( b ) |
    a2 r2

    \barNumberCheck 59
    r2 a2 |
    h!1 \< ( |
    c2 ) \! h |
    b1~ |
    b2 b2 |
    as2 \> g |
    r2 \! g4 f |
    e!1~
    e1~ |
    e1~ \< |
    e2. \! e4 |
    <> \> \after 2. \! e1 \bar "||"\barFermata
  }
}

vexilla_three_soprano = \relative c' {
  \set Score.currentBarNumber = 71
  e1^\p | 
  e2 \< f \! |
  f2 (  e2~ e4 d4 ) g ( \>  f ) \! |
  e2 ( c4 d |
  e f^\crescsempre e8 g a h |
  c4 a8 h c4 ) d |
  d2 r2 |

  \barNumberCheck 79
  dis2 ^\f fis  (
  cis ) dis |
  e d! ( |
  c!2.  ) c4 |
  h2 e,^\dimsempre |
  fis2. fis4 |
  gis2 r2

  \barNumberCheck 86
  r2 a2~^\p ( |
  a1 \< \after 8 \! b2 ) a | 
  d1->
  d2 \> c 
  c1~ \! c4 ( b4 ) a ( b )
  a2 r2 |

  \barNumberCheck 94
  r2 a2 ( |
  h!1 \<
  c2 \! ) h 
  b1 
  b2 b 
  as4. \> as8 g2 \!

  \barNumberCheck 100
  r2 g4 f  | % 101
  e1~ e~
  e1~ \< e1 \> e1\fermata^\pp \fine
}
