vexilla_one_alto = \relative c' {
          e2^\p ( d4 c4 ) |
          h2 \< c |
          c1 ( \! |
          a2 ) \> a2 |
          e'2 \! ( c4 d |
          e4 f e f |
          e2.^\crescsempre ) fis4 |
          g2 r2 |

          \barNumberCheck 9
          fis2^\f fis |
          fis2 fis |
          e2 ( g |
          e2. ) e4 |
          dis2^\dimsempre e4 e |
          e2. dis4 |
          e2 r2 |
          r1 |
          r2 f2^\p \< |
          g2^>  \! f  |

          \barNumberCheck 19
          a2^> ( d,4 e |
          f2 ) \> f |
          d1~ ^\p |
          d2 d |
          c2 r2 |
          r2 f4( e) |
          dis1\< \after 8 \! dis2 dis2
          es1 (
          d2 ) d2 \> |

          \barNumberCheck 29
          c1~ | c2. \!  \breathe c4 |
          c1 ( |
          d2 \< c |
          h1 \! a2. ) h4 \> \after 4 \!
          h1
	  \bar "||" \barFermata
        }

vexilla_two_alto = \relative c' {
  \barNumberCheck 36
  e2^\p \< ( d4 c |
  h2 ) \! c |
  c1 |
  a \> |
  e'2 \! c4 ( d |
  e f e^\crescsempre f |
  e2. ) fis4 |
  g2 r2 |

  \barNumberCheck 44
  fis^\f fis4. fis8 |
  fis2 fis |
  e ( g |
  e2. )  e4 |
  dis2^\dimsempre e |
  e2. dis4 |
  e2 r2 |
  r1 |

  \barNumberCheck 52
  r2 f^\p ( \< \after 4 \!
  g2  ) f  |
  a2 ( d,4 e ) |
  f2 \> f |
  \after 2 \! d1~ |
  d2 d2 |
  c2 r2 |

  \barNumberCheck 59
  r2 f4 ( e ) |
  dis1~ \< |
  dis2 \! dis2 |
  es1 ( |
  d2 ) d |
  c1~ \> |
  c2. \! c4 |
  c1 ( |
  d2 c |
  h1 \< |
  a2. \! ) h4 | 
  <> \> \after 2. \! h1 \bar "||"\barFermata
}

vexilla_three_alto = \relative c' {
  \barNumberCheck 71
  e2^\p ( d4 c )
  h2 \< c \!
  c1 (
  a2 ) a \>
  | % 75
  e' \! ( c4 d 
  | % 76
  e f^crescsempre e f 
  | % 77
  e2. ) fis4 
  | % 78
  g2 r2

  \barNumberCheck 79
  fis^\f fis2~ |
  fis2 fis2 e 
  g ( e2. ) e4 dis2 e^\dimsempre e2. dis4 
  e2 r2

  \barNumberCheck 86
  r1 |
  r2 f2^\p ( \< \after 8 \! g )
  f |
  a^> ( d,4 e )
  f2 \> f
  d1~ \! |
  d2 d2 c 
  r2 | % 94

  \barNumberCheck 94
  r2 f4 ( e |
  dis1 \<
  dis2 \! )  dis2 es1 d2 d 
  | % 99
  <>\> \after 2 \! c1~ c2. c4 c1 d2 c 
  | % 103
  h1\< (
  | % 104
  a2. \> h4 )
  | % 105
  h1\fermata\pp\fine
}
