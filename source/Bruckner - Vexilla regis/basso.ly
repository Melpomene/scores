vexilla_one_basso = \relative c {
          e1^\p |
          g2 \<  f |
          c1 \! ( |
          d2 ) \> d |
          e4 \! ( h c d |
          e4 f e d |
          a2^\crescsempre  a'4 ) a4 |
          g2 r2 |

          \barNumberCheck 9
          h,2^\f dis4 ( e ) |
          fis2 h, |
          c2 ( g |
          a2. ) a4 |
          h2^\dimsempre c4 h |
          a2 ( h4 ) h |
          <e, \tweak font-size -3 e'>2 r2 |
          r2 a'2^\p |
          g2 \< ( f |
          e2-> ) \! f |

          \barNumberCheck 19
          d1-> (|
          f,2 ) \> f |
          b1~^\p |
          b2 d |
          f2 r2 |
          r2 f |
          fis1\< \after 8 \! fis2 fis |
          g1 ( |
          b,2 ) b \> |

          \barNumberCheck 29
          c1  \breathe |
          as1 \! |
          c1 ( \( h!2 \< a! \) |
          <e e'>1~ \! |
          <e e'>2. ) <e e'>4 \> \after 4 \!
          <e e'>1
	  \barFermata
        }

vexilla_two_basso = \relative c {
  \barNumberCheck 36
  e1^\p \< |
  g2 \! (  f ) |
  c1  |
  d1  \> |
  e4 \! ( h ) c4 ( d |
  e4 f e^\crescsempre d | 
  a2 a'4 ) a |
  g2 r2 |

  \barNumberCheck 44
  h,^\f dis4 e |
  fis2 h, |
  c ( g |
  a2. ) a4 |
  h2^\dimsempre c4 ( h ) |
  a2 ( h4 ) h |
  <e, e' >2 r2 |

  \barNumberCheck 51
  r2 a'2^\p ( |
  g2 \< f 
  \after 8 \! e2 ) f
  d1 |
  f,2 \> f 
  \after 2 \! b1~  |
  b!2 d2 |
  f2 r2 |

  \barNumberCheck 59
  r2 f2 |
  fis1~ \< |
  fis2 \! fis2 |
  g1 ( |
  b,2 ) b |
  c1 \> (  |
  as2. \! ) as4 
  c1 ( |
  h!2 a |
  <e e' >1 \< <e e' >2. \! )  <e e' >4 |
  <>  \> \after 2. \! <e e' >1 \bar "||" \barFermata 
}

vexilla_three_basso = \relative c {
  \barNumberCheck 71
  e1^\p |
  | % 72
  g2 \< f \!
  | % 73
  c1 (
  | % 74
  d2 ) d \>  
  | % 75
  e4 \! ( h c d 
  | % 76
  e f^\crescsempre e d 
  | % 77
  a2 a'4 ) a 
  | % 78
  g2 r2

  \barNumberCheck 79
  h,^\f dis4 ( e 
  fis2 )  h, 
  c g (
  a2. ) a4 
  h2 ( c4^\dimsempre ) h 
  a2 ( h4  ) h 
  <e, \tweak font-size -3 e' >2 r2 |

  \barNumberCheck 86
  r2 a'2^\p ( g \<
  f \after 8 \! e )
  f d1-> f,2 \> f 
  b1~ \! | b2 d2 f 
  r2  |

  \barNumberCheck 94
  r2 f2 ( fis1 \< fis2 \! ) fis2 g1 b,2 b
  <>\> \after 2 \! c1 (
  as2. ) as4 
  | % 101
  c1

  \barNumberCheck 102
  h!2 a!
  <e e' >1~ \<
  <e e' >1 \>
  <e e' >1\fermata^\pp
  \fine
}
