vexilla_one_tenore = \relative c {
          e1^\p |
          h'2 \<  a |
          g1 \! ( |
          f2 ) \> d' |
          e,2 \! ( c4 d |
          e4 f e d' |
          c4.^\crescsempre  d8 e4 ) c |
          h2 r2 |

          \barNumberCheck 9
          h2^\f h |
          ais h |
          g2 ( h~ |
          h2 a!4 ) g |
          fis2^\dimsempre e4 e |
          c'2( h4 ) h | 
          h2 r2 |
          r2 a2^\p |
          a1 ( \< |
          des2-> \! ) c |

          \barNumberCheck 19
          f2-> ( f,4 g | a2 ) \> a |
          f1~ ^\p |
          f2 f |
          f2 r2 |
          r2 a |
          a1\< \after 8 \! 
          a2 a |
          b1 ( |
          as2 ) g4 ( \> f )

          \barNumberCheck 29
          es1~
          es2. \! \breathe f4 |
          g1 ( |
          e!2 \< a! \! |
          h1 |
          c2. ) h4 \> \after 4 \!
          h1
	  \barFermata
        }

vexilla_two_tenore = \relative c {
  \barNumberCheck 36
  e1^\p \< | 
  h'2\! ( a ) |
  g1 ( |
  f2 ) \>  \after 2 \! d' |
  e, c4 (  d |
  e f e^\crescsempre d' |
  c4. d8 e4 ) c |
  h2 r2 |

  \barNumberCheck 44
  h^\f  h4. h8 |
  ais2 h |
  g ( h2~ |
  h2 a!4 ) g |
  fis2^\dimsempre  e |
  c'2 ( h4 ) h |
  h2 r2 |

  \barNumberCheck 51
  r2 a2^\p ( |
  a1 \< \after 4 \! 
  des2 ) c 
  f ( f,4 g )|
  a2 \> a |
  \after 2 \! f1~ |
  f2 f2 |
  f2 r2 |

  \barNumberCheck 59
  r2 a2 |
  a1~ \< |
  a2 \! a2 |
  b1 ( |
  as2 ) g4 ( f ) |
  es1~ \> |
  es2. \! f4 |
  g1 ( e!2 a! | 
  h1 \< |
  c2. \! ) h4 |
  <> \> \after 2. \! h1 \bar "||" \barFermata
}

vexilla_three_tenore = \relative c {
  \barNumberCheck 71
  e1^\p
  | % 72
  h'2 \< a \!
  | % 73
  g1 (
  | % 74
  f2 ) d' \> 
  | % 75
  e, \! ( c4 d 
  | % 76
  e f^\crescsempre e d' 
  | % 77
  c4. d8 e4 ) c 
  | % 78
  h2 r2

  \barNumberCheck 79
  h1^\f 
  | % 80
  ais2 h
  g h2~ | h2 ( a!4 ) g fis2 
  e^\dimsempre c' (
  h4 ) h h2 r2

  \barNumberCheck 86
  r2 a2~^\p  ( 
  a1  \<  \after 8 \! des2 ) c 
  f-> ( f,4 g )
  a2 \> a 
  f1~ \! f2 f2 f 
  r2

  \barNumberCheck 94
  r2 a2~ a1~ \< a2 \! a2 b1 as2 g4 ( f )
  <> \> \after 2 \! es1~ es2. f4 g1 |

  \barNumberCheck 102
  e!2 a!
  h1 ( \<
  | % 104
  c2. \> h4 )
  | % 105
  h1^\fermata^\pp\fine
}
