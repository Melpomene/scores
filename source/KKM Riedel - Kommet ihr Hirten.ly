\version "2.20.0"
\language "deutsch"

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}
    
\header {
  title = "Kommet, ihr Hirten"
  poet = "Altböhmisches Weihnachtslied"
  composer = "Carl Riedel (1827–1888)"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
    \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key g \major
  \time 3/4
  \autoBeamOff
  \dynamicUp
}

%% -----------------------------------------------------------------------------------------
%% I. Die Engel

sopranoWordsI = \lyricmode {
  Kom -- met, __ ihr __ Hir -- ten, __ ihr __ Män -- ner __ und Frau'n,
  kom -- met, __ das __ lieb -- li -- che __ Kind -- lein __ zu __ schaun.
  Chris -- tus, der Herr, ist heu -- te ge -- bo -- ren,
  den Gott zum Hei -- land euch hat er -- ko -- ren.
  Fürch -- tet __ euch nicht!
}
altoWordsI = \lyricmode {
  Kommt, ihr Hir -- ten, Mann und Frau'n,
  kommt, das lie -- be Kind zu schaun.
}

sopranoIMusicI = \relative c'' {
  \tempo "Sanft, nicht zu schnell"
  \clef "treble"
  d4^\mf d8[ h] e[ c] | d4 d8[ h] e[ c] | d4 h8[ d] a[ \> h] | g2 \! h4\rest |
  d4^\p  d8[ h] e[ c] | d4 d8[ h] e[ c] | d4 h8[ d] a[ \> h] | g2 \! h4\rest |
  g4^\f h8 g h-> d | g,4 h8 g a-> d, | 
  g4^\mf h8 g h^\markup{ \center-align \italic { etwas breit } } d |
  g,4 h8 g a-> d, |
  d'4^\p^\markup{ \italic ruhig } h8[ d] a[ h] | g2 h4\rest \bar "||"
}

sopranoIIMusicI = \relative c'' {
  \clef "treble"
  h4 h8[ g] c[ a] | h4 h8[ g] c[ a] | h4 g8[ h] a[ d,] g2 s4 |
  h4 h8[ g] c[ a] | h4 h8[ g] c[ a] | h4 g8[ h] a[ d,] g2 s4 |
  g4 h8 g h d | g,4 h8 g a d, | g4 h8 g h d | g,4 h8 g a d, |
  g4 g fis | d2 s4 |
}

altoMusicI = \relative c'' {
  \clef "treble"
  g2^\mf g4 | g2 g4 | g2 fis4 \> | g2 \! h4\rest |
  g2^\p g4 | g2 g4 | g2 fis4 \> | g2 \! h4\rest |
  g4^\f h8 g h-> d | g,4 h8 g a-> d, | g4^\mf h8 g h d | g,4 h8 g a-> d, |
  h4^\mf d c | h2 h'4\rest
}


\score {
  \new ChoirStaff <<
    \new Staff = "sopranos"
    <<
      \new Voice = "sopranoI" {
	\global
	\voiceOne
	\sopranoIMusicI
      }
      \new Voice = "sopranoII" {
	\global
	\voiceTwo
	\sopranoIIMusicI
      }
    >>
    \new Lyrics \lyricsto "sopranoI" {
      \sopranoWordsI
    }
    \new Staff = "altos" 
    <<
      \new Voice = "alto" {
	\global
	\altoMusicI
      }
    >>
    \new Lyrics \lyricsto "alto" {
      \altoWordsI
    }
  >>
  \header { 
    piece = "I. Die Engel"
  }
}

%% -----------------------------------------------------------------------------------------
%% II. Die Hirten

sopranoMusicII = \relative c'' {
  \dynamicUp
  d4^\mf d8[ h] e[ c] | d4 d8[ h] e[ c] | d4 h8[ d] a[ h] | g2 h4\rest |
  d4^\mp d8[ h] e[ c] | d4 d8[ h] e^\p[ c] | d4 h8[ d] a[ h] | g2 h4\rest |
  g4^\f^\markup{ \italic klangvoll } h8 g h \>  d \! | g,4-> h8 g a-> d, |
  g4^\mf h8 g h \<  d \! | g,4^\p h8 g a-> d,| 
  d'4^\f h8->[ d] a[ h] | g2 g4\rest \bar "||"
}

altoMusicII = \relative c'' {
  h4^\mf h8[ g] c[ a] | h4 h8[ g] c[ a] | g4 g fis | g2 r4 |
  g4^\mp h8[ g] c[ a] | h4 h8[ g] c^\p [ a] | g4 g fis | g2 r4 |
  d4^\f g8 d g \> h \! | d,4-> g8 d fis-> d |
  d4 g8 d g \< h d,4^\p g8 d fis-> d |
  g4^\f g-> fis | d2 r4 |
}

tenorMusicII = \relative c' {
  \clef "G_8"
  g4^\mf d' fis | g d a | h d c | h2 r4 |
  h^\mp d fis | g d a^\p | h d c | h2 r4 |
  h4^\f g8 h d \> g \! h,4-> d8 h c-> c | h4 g8 h d\< g |
  h,4^\p d8 h c-> c |
  h4^\f d-> c | h2 r4
}

bassMusicII = \relative c' {
  \clef "bass"
  g2^\mf g4 | g2 g4 | g2 d4 | g2 r4 |
  g2^\mp g4 | g2 g4^\p | g2 d4 | g2 d4\rest |
  d2.^\markup { { \dynamic mf } dolce } | d | d~ | d | d~ | d2 d4\rest
}

sopranoWordsII = \lyricmode {
  Las -- set __ uns __ se -- hen __ in __ Beth -- le -- hems Stall.
  was uns __ ver -- hei -- ßen __ der __ himm -- li -- sche __ Schall,
  was wir dort fin -- den, las -- set uns kün -- den,
  las -- set uns prei -- sen in from -- men Wei -- sen.
  Hal -- le -- lu -- ja!
}
altoWordsII = \sopranoWordsII

tenorWordsII = \lyricmode {
  Las -- set uns se -- hen in Beth -- le -- hems Stall,
  was uns ver -- hei -- ßen der himm -- li -- sche Schall,
  was wir dort fin -- den, las -- set uns kün -- den,
  las -- set uns prei -- sen in from -- men Wei -- sen.
  Hal -- le -- lu -- ja!
}

bassWordsII = \lyricmode {
  Lass -- set sehn in Beh -- l'hems Stall,
  was ver -- hei -- ßen Himm -- mels Schall.
  Hal -- le -- lu -- ja! __
}

\score {
  \new ChoirStaff <<
    \new Staff = "sopranos"
    <<
      \new Voice = "IIsopranos" {
	\set Score.currentBarNumber = #15
	\global
	\sopranoMusicII
      }
    >>
    \new Lyrics \lyricsto "IIsopranos" {
      \sopranoWordsII
    }
    \new Staff = "altos" <<
      \new Voice = "IIaltos" {
	\global
	\altoMusicII
      }
    >>
    \new Lyrics \lyricsto "IIaltos" {
      \altoWordsII
    }
    \new Staff = "tenors" <<
      \new Voice = "IItenors" {
	\global
	\tenorMusicII
      }
    >>
    \new Lyrics \lyricsto "IItenors" {
      \tenorWordsII
    }
    \new Staff = "bassos" <<
      \new Voice = "IIbassos" {
	\global
	\voiceOne
	\bassMusicII
      }
      \new Voice {
	\global
	\voiceTwo
	s2.*8 | g,2.| g, | g,~ | g, | g,~ | g,2 s4
      }
    >>
    \new Lyrics \lyricsto "IIbassos" {
      \bassWordsII
    }
  >>
  \header {
    piece = "II. Die Hirten (Doppelquartett)"
  }
}

%% -----------------------------------------------------------------------------------------
%% III. Das Volk

sopranoMusicIII = \relative c'' {
  \dynamicUp
  d4^\f d8[ h] e[ c] | d4 d8[ h] e[ c] | d4 h8[ d] a[ h] | g2 h4\rest |
  d4 d8[ h] e[ c] | d4 d8[ h] e[ c] | d4->  h8->[ d->] a->[ h->] | g2 h4\rest |
  g4^\mf h8 g h \>  d | g,4^\p h8 g a d, |
  g4^\mf h8 g h-- d-- | g,4 h8 g a-> d,| 
  d'4^\f h8[ \< d] a[ h] | g2 \! h4\rest \bar "|."
}

altoMusicIIIa = \relative c'' {
  g4^\f h8[ g] c[ a] | h4 h8[ g] c[ a] | g4 g fis | g2 h4\rest |
  g4 h8[ g] c[ a] | h4 h8[ g] c [ a] | g4-> g8->[ h8->] fis4 | g2 h4\rest |
  d,4^\mf g8 d g \> h \! | d,4^\p g8 d fis d |
  d4^\mf g8 d g-- h-- d,4 g8 d fis-> d |
  g4^\f g\<  fis | d2 \! h4\rest |
}
altoMusicIIIb = \relative c' {
  s2.*8 |
  h4 d8 h d g | h,4 d8 h c c | h4 d8 h d g | h,4 d8 h c c |
  h4 d c | h2 s4 |
}

tenorMusicIII = \relative c' {
  \clef "G_8"
  h4^\f d fis | g d a | h d c | h2 r4 |
  h d fis | g d a | h d c | h2 r4 |
  h4^\mf d8 h d \> g \! h,4^\p d8 h c c |
  h4^\mf d8 h d-- g-- | h,4 d8 h c-> c |
  h4^\f d \< c | h2 \! r4
}

bassMusicIII = \relative c' {
  \clef "bass"
  g2^\f g4 | g2 g4 | g2 d4 | g2 d4\rest |
  g2 g4 | g2 g4 | g2 d4 | g2 d4\rest |
  g2.^\markup { { \dynamic mf } dolce } g^\p g^\mf~ g | g~^\f \< | g2 \! d4\rest |
}

sopranoWordsIII = \lyricmode {
  Wahr -- lich, die __ En -- gel __ ver -- kün -- di -- gen heut
  Beth -- le -- hems Hir -- ten -- volk __ gar gro -- ße __ Freud'.
  Nun soll es wer -- den Frie -- de auf Er -- den,
  den Men -- schen al -- len ein Wohl -- ge -- fal -- len.
  Eh -- re __ sei __ Gott!
}
altoWordsIII = \lyricmode {
  Wahr -- lich, die __ En -- gel __ ver -- kün -- di -- gen heut
  Beth -- le -- hems Hir -- ten -- volk __ gar gro -- ße Freud'.
  nun soll es wer -- den Frie -- de auf Er -- den,
  den Men -- schen al -- len ein Wohl -- ge -- fal -- len.
  Eh -- re sei Gott!
}

tenorWordsIII = \lyricmode {
  Wahr -- lich, die __ En -- gel __ ver -- kün -- di -- gen heut
  Beth -- le -- hems Hir -- ten -- volk gar gro -- ße Freud'.
  nun soll es wer -- den Frie -- de auf Er -- den,
  den Men -- schen al -- len ein Wohl -- ge -- fal -- len.
  Eh -- re sei Gott!
}

bassWordsIII = \lyricmode {
  Wahr, die En -- gel kün -- den heut
  Beth -- l'hems Hir -- ten gro -- ße Freud',
}

\score {
  \new ChoirStaff <<
    \new Staff = "sopranos"
    <<
      \new Voice = "IIIsopranos" {
	\set Score.currentBarNumber = #29
	\global
	\sopranoMusicIII
      }
    >>
    \new Lyrics \lyricsto "IIIsopranos" {
      \sopranoWordsIII
    }
    \new Staff = "altos" <<
      \new Voice = "IIIaltos" {
	\global
	\voiceOne
	\altoMusicIIIa
      }
      \new Voice {
	\global
	\voiceTwo
	\altoMusicIIIb
      }
    >>
    \new Lyrics \lyricsto "IIIaltos" {
      \altoWordsIII
    }
    \new Staff = "tenors" <<
      \new Voice = "IIItenors" {
	\global
	\tenorMusicIII
      }
    >>
    \new Lyrics \lyricsto "IIItenors" {
      \tenorWordsIII
    }
    \new Staff = "bassos" <<
      \new Voice = "IIIbassos" {
	\global
	\voiceOne
	\bassMusicIII
      }
      \new Voice {
	\global
	\voiceTwo
	\relative c { s2.*8 | d2. | d | d~ d | d~ d2 s4 | }
      }
      \new Voice {
	\global
	\voiceTwo
	\relative c { s2.*8 | g2. | g | g~ g | g~ g2 s4 | }
      }
    >>
    \new Lyrics \lyricsto "IIIbassos" {
      \bassWordsIII
    }
  >>
  \header {
    piece = "III. Tutti: Das Volk"
  }
}

