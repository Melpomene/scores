\version "2.20.0"
\language "deutsch"

\paper {
  page-count = #6
}

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}

\header {
  title = "Frohlocket, ihr Völker auf Erden"
  subtitle = "Nr. 2 aus Sechs Sprüche zum Kirchenjahr op. 79 (1845)"
  poet = "Text: Psalm 47"
  composer = "Musik: Felix Mendelssohn Bartholdy (1809–1847)"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
      \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key g \major
  \time 4/4
}


voiceSI = \relative c'' {
  \clef "treble"
  \partial 4
  \tempo "Allegro moderato"
  \autoBeamOff
  g4^\f | h c8 c8 d4 g,8 g | a2 h4 d | e2. e4 | d2 r |
  r2 r4 g | fis e d d | e d c c | c4. c8 h4 d | g2 cis, | d a |
  %% Er hat seine Gerechtigkeit (11)
  d4^\f d d d8 d | d4. d8 d4 d | c!2 c4 c | f1 | e2
  e4 e | e( d) c2 | r2 c4 c | c( h ) a2 |
  h4^\f h h h8 h | c4. c8 c4 c | f2 f4 f |
  f2 e | f4 e d2 | c c4 c | c( h ) a( g) | fis g a2 d,2
  e'4 e | d a h c | h2 a4
  %% Frolocket
  g4 | h c8 c d4 g,8 g a2 h4^\f h8 h | e1 | d\fermata \bar "|."
}
voiceSII = \relative c'' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  g4^\f | h c8 c8 d4 g,8 g | a2 h4 d | d2 c | h r4
  g | d' c h h | c2 h4 h | e e e e | d4. d8 d4 d | d2 cis d a
  %% Er hat seine Gerechtigkeit (11)
  h4^\f h h d8 d | g,4. g8 g4 g | c!2 c4 c | d1( | e4 d ) c r |
  r2 e4 e | e( d ) c2 | R1 |
  g4^\f g g g8 g | c4. c8 c4 c | c2 c4 c |
  %%
  b2( c ) | c r | R1 | r2
  c4 c | c( h) a( g) | fis g c2~ | c4 d4 
  d4 c | h2 a4
  %% Frolocket
  g | h c8 c d4 g,8 g | a2
  h4^\f d8 d | d2( c) | h1 \fermata
}

voiceAI = \relative c'' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  g4^\f | h c8 c d4 g,8 g | a2 h4 h | h( a) g2 | g4
  g fis e | d e d2( | fis) g4 g | g g a a | a4. a8 g4 d | h'2 g | g fis
  %% Er hat seine Gerechtigkeit (11)
  fis4^\f fis fis h8 h | h4. h8 h4 h | a2 a4 a | a2( g) | g2
  c4 h | a( h) c2 | r2 a4 a | a( g) fis2 |
  g4^\f g g g8 g | g4. g8 g4 g | f2 f4 f
  %%
  g2 g | a4 a h!2 | c a4 g | fis!( g fis e ) | d2 a'4 a | a( g) fis( e)
  a2( d,4 ) a' | g2 fis4
  g | h c8 c d4 g,8 g | a2 h4^\f h8 h | h4( a) g2~ |  g1\fermata
}

voiceAII = \relative c'' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  g4^\f | h c8 c d4 g,8 g | a2 h4 g | g( fis) e( fis) | g 
  g fis e | d e d2( | c) d4
  d | g g e e | a4. a8 d,4 a' | g2 e | a d,
  %% Er hat seine Gerechtigkeit (11)
  d4^\f d d d8 d | e4. e8 e4 g | g2 g4 c | c2( h | g4 h ) a r4 |
  r2 c4 h | a( h a g | fis g ) d2 |
  d4^\f d d d8 d | c4. c8 c4 c | a'2 a4 a
  %%
  g2 g | r2 h4 a | g2 e | r2 a4 a | a( g fis e) | d2 fis4 ( g
  a2 g4 ) g | g2 d4
  g4 | h c8 c d4 g,8 g | a2 d,4^\f g8 g | g4( fis e fis) | g1\fermata
}

voiceTI = \relative c' {
  \clef "G_8"
  \partial 4
  \autoBeamOff
  r4 r1 r2 r4
  h^\f | e2 e | h r4
  g^\f h c8 c d4 g,8 g | a2 h4 d | c d e a, | a4. d8 d4 d | g,2 a | a d
  %% Er hat seine Gerechtigkeit (11)
  d4^\f d d d8 d | d4. d8 d4 e | e2 e4 e | f2( g) | g
  e4 g | fis!( g) c,2 | r2 e4 e | a, ( h ) c( d)
  d4^\f h h h8 h | a4. a8 e'4 e | d2 d4 d
  %%
  d2 c | c4 c d2 | e2 c4 c | a( h c e)| a, r a a | a( d) c2~ |
  c4 c( h) a | h( c) d2~(  | d g~ | g4 fis )
  g^\f d e1 | g, \fermata
}

voiceTII = \relative c' {
  \clef "G_8"
  \partial 4
  \autoBeamOff
  r4 r1 r2 r4
  g^\f | c2 c | d r4
  g,^\f h c8 c d4 g,8 g | a2 h4
  g | g g c c | d4. a8 h4 a | h2 cis a a
  %% Er hat seine Gerechtigkeit (11)
  h4^\f fis fis fis8 fis | g4. g8 g4 g | e'2 a,4 a | a2 ( h) | c r |
  r2 e4 e | c g a a | d2 d, |
  d4^\f d e e8 e | a4. a8 e4 c' | f,2 f4 f
  %%
  b1 | a2 r | e'4 d c2 | d a4 a | a ( h c e ) | a, r a a |
  a d d e | d2 ( c ) | h1( | d2)
  h4^\f h | c1 d\fermata
}

voiceBI = \relative c' {
  \clef "bass"
  \partial 4
  \autoBeamOff
  r4 r1 r2 r4
  g^\f | g2 g4( a) | h2 r4
  g^\f | h c8 c d4 g,8 g | a2 h4
  h | c h a g | fis4. fis8 g4 fis | e2 e | d d
  %% Er hat seine Gerechtigkeit (11)
  h'4^\f h h h8 h h4. h8 h4 h | c!2 a4 a | d1 | c2
  c4 c | c( h a g) | fis( g ) c, c | d2 d |
  g4^\f g e e8 e | e4. e8 a4 a | a2 a4 a
  %%
  b2 g | f4 f f2 | e e4 e | d2 d~ | d c'4 c | c( h ) a( g) |
  fis2( g4) c, | d1~ | d2( d') | c
  h4^\f h | g2.( a4) | h1\fermata
}

voiceBII = \relative c' {
  \clef "bass"
  \partial 4
  \autoBeamOff
  r4 r1 r2 r4
  g^\f | c,2 c | g2 r4
  g'^\f | h c8 c d4 g,8 g | a2 h4
  h,| c h a g | fis4. fis'8 g4 fis | e2 e | d d
  %% Er hat seine Gerechtigkeit (11)
  h4^\f h h h8 h | e4. e8 e4 e | a2 a4 a | d,2 ( g, ) | c r |
  r2 c'4 c c( h) a2 | R1 | 
  g,4^\f g g g8 g a4. a8 a4 a | d2 d4 d
  %%
  g2( c, ) | f2 r | r a,4 a | d1 | d2 c4 c | c4 ( h a g |
  fis) fis  g c | d1 g,1~ | g2
  g'4^\f g | c,1 | g\fermata
}


wordsSI = \lyricmode {
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den, und prei -- set Gott!
  Der Hei -- land ist er -- schie -- nen, ist er -- schie -- nen,
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- recht -- tig -- keit der Welt of -- fen -- bar -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja, __
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja! 
  Froh -- lo -- cket, ihr Völ -- ker auf Er -- den. Hal -- le -- lu -- ja!
}
wordsSII = \lyricmode {
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den, und prei -- set Gott!
  Der Hei -- land ist er -- schie -- nen, der Hei -- land ist er -- schie -- nen,
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja! 
  Froh -- lo -- cket, ihr Völ -- ker auf Er -- den. Hal -- le -- lu -- ja!
}
wordsAI = \lyricmode {
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den, und prei -- set Gott!
  Der Hei -- land ist er -- schie -- nen, der Hei -- land ist er -- schie -- nen,
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,  
  Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja!
  Froh -- lo -- cket, ihr Völ -- ker auf Er -- den. Hal -- le -- lu -- ja! __
}
wordsAII = \lyricmode {
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den, und prei -- set Gott!
  Der Hei -- land ist er -- schie -- nen, der Hei -- land ist er -- schie -- nen,
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja! 
  Froh -- lo -- cket, ihr Völ -- ker auf Er -- den. Hal -- le -- lu -- ja!
}

wordsTI = \lyricmode {
  und prei -- set Gott!
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den!
  Der Hei -- land ist er -- schie -- nen, 
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja! __
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja, __
  Hal -- le -- lu -- ja, __  Hal -- le -- lu -- ja!
}
wordsTII = \lyricmode {
  und prei -- set Gott!
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den!
  Der Hei -- land ist er -- schie -- nen, 
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,  Hal -- le -- lu -- ja,
  Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja! 
}

wordsBI = \lyricmode {
  und prei -- set Gott!
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den!
  Der Hei -- land ist er -- schie -- nen, 
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja, __
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja! 
}
wordsBII = \lyricmode {
  und prei -- set Gott!
  Froh -- lo -- cket, ihr Völ -- der auf Er -- den!
  Der Hei -- land ist er -- schie -- nen, 
  den der Herr ver -- hei -- ßen.
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja!
  Er hat sei -- ne Ge -- rech -- tig -- keit der Welt of -- fen ba -- ret.
  Hal -- le -- lu -- ja, Hal -- le -- lu -- ja,
  Hal -- le -- lu -- ja, __ Hal -- le -- lu -- ja! 
}


\new ChoirStaff <<
  \new StaffGroup << % Soprano
    \new Staff = "SI" 
    <<
      \new Voice = "SI" { \global \voiceSI }
    >>
    \new Lyrics \lyricsto "SI" { \wordsSI }
    \new Staff = "SII"
    <<
      \new Voice = "SII" { \global \voiceSII }
    >>
    \new Lyrics \lyricsto "SII" { \wordsSII }
  >> % end StaffGroup Soprano

  \new StaffGroup << % Alto
    \new Staff = "AI" 
    <<
      \new Voice = "AI" { \global \voiceAI }
    >>
    \new Lyrics \lyricsto "AI" { \wordsAI }
    \new Staff = "AII"
    <<
      \new Voice = "AII" { \global \voiceAII }
    >>
    \new Lyrics \lyricsto "AII" { \wordsAII }
  >> % end StaffGroup Alto

  \new StaffGroup << % Tenor
    \new Staff = "TI" 
    <<
      \new Voice = "TI" { \global \voiceTI }
    >>
    \new Lyrics \lyricsto "TI" { \wordsTI }
    \new Staff = "TII"
    <<
      \new Voice = "TII" { \global \voiceTII }
    >>
    \new Lyrics \lyricsto "TII" { \wordsTII }
  >> % end StaffGroup Tenor

  \new StaffGroup << % Bass
    \new Staff = "BI" 
    <<
      \new Voice = "BI" { \global \voiceBI }
    >>
    \new Lyrics \lyricsto "BI" { \wordsBI }
    \new Staff = "BII"
    <<
      \new Voice = "BII" { \global \voiceBII }
    >>
    \new Lyrics \lyricsto "BII" { \wordsBII }
  >> % end StaffGroup Bass
>>  % end ChoirStaff