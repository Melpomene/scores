\version "2.24.3"
\language "deutsch"

#(set-default-paper-size "a4")

\paper {
%  indent = 0\cm
}

\header {
  title = "O Haupt voll Blut und Wunden"
  subtitle = "EKG 85"
  composer = "Johann Crüger, 1649"
  poet = "Paul Gerhardt, 1656"
  % arranger = "Heike Kiefner-Jesatko"
  tagline = "Konkordien-Kantorei Mannheim 2024 (rev. 1) – Vervielfältigung und Verbreitung erlaubt"
}

global = {
  \key c \major
  \time 4/4
  \partial 4
}


\score {
  \new ChoirStaff <<
    \new Staff = women <<
      \new Voice = "soprano" { 
	\voiceOne
        \global
	\relative c' { e4 \repeat volta 2 { a4 g  f e | d2  e4 \breathe
					    h'4  | c4 c h h |
					    \alternative {
					      \volta 1 { a2 s4 e }
					      \volta 2 { a2 s4 c }
					    }
					  }
			  h4 g a h c2 c4 \breathe
			  g a g a f e2 s4
			  c' h4 d c h a2 h4 \breathe
			  e, f e d d | e2. \fine
			}
      }
     \new Voice = "alto" {
	\voiceTwo
       \global
	\relative c' { g4 \repeat volta 2 { \relative c'  a4 c a8( h)  c4~ | c8 ( h16 a) h4 c4 \breathe
					d e4 e e e 
					\alternative {
					  \volta 1 { e2 r4 g, }
					  \volta 2 { e'2 r4 e }
					}
				      }
		       g4 c, c d e2 e4 \breathe
		       e f e f d cis2
		       r4 e e g e d d2 d4 \breathe
		       c4 d c c h c2. \fine
		     }
     }
    >>
    \new Lyrics {
      \lyricsto "soprano" {
	\set stanza = "2. "
	\lyricmode { Du e -- dles An -- ge -- si -- chte, da -- vor sonst schrickt und scheut das }
      }
    }
    \new Lyrics {
      \lyricsto "soprano" {
	\lyricmode {
	  \skip 1
	  gro -- ße Welt ge -- wi  -- chte: wie bist du so be -- \repeat unfold 2 { \skip 1 } speit,
	  wie   bist du so er -- blei -- chet! Wer hat dein Au -- gen -- licht,
	  dem sonst kein Licht nicht glei -- chet,
	  so schänd -- lich zu -- ge -- richt?
	}
      }
    }
    \new Lyrics {
      \lyricsto "soprano" {
	\set stanza = "6. "
	\lyricmode { Ich will hier bei dir ste -- hen, ver -- ach -- te mich doch nicht; von }				%	\repeat unfold 2 { \skip 1 }
      }
    }
    \new Lyrics {
      \lyricsto "soprano" {
	\lyricmode {
	  \skip 1
	  dir will ich nicht ge -- hen, wenn dir dein Her -- ze
	  \repeat unfold 2 { \skip 1 } bricht; 
	  wenn dein Haupt wird er -- blas -- sen im letz -ten To -- des -- stoß,
	  als dann will ich dich fas -- sen in mei -- nem Arm und Schoß. }
      }
    }
    \new Staff = men <<
      \clef "bass"
      \new Voice = "tenor" {
	\voiceOne
	\global
	\relative c' { c4 \repeat volta 2 { c4 c d g, g2 g4 \breathe
					    g g4 a a gis 
					    \alternative {
					      \volta 1 { a2 s4 c }
					      \volta 2 { a2 s4 a }
					    }
					  }
		       d4 c a f g2 g4 \breathe
		       c c c c a a2
		       s4 a4 g g g g g ( fis ) g \breathe
		       g g g g g g2. \fine
		     }
      }
      \new Voice = "bass" {
	\voiceTwo
	\global
	\relative c { c4 \repeat volta 2 { f4 e d c g2 c4 \breathe
					   g c4 a e' e 
					   \alternative {
					     \volta 1 { a,2 r4 c }
					     \volta 2 { a2 r4 a' }
					   }
					 }
		      g4 e f d c2 c4 \breathe
		      c f c f, d' a2 r4
		      a | e' h c g | d'2 g,4\breathe
		      h4 | h! c g g | c2.
		    }
      }
    >>
  >>
}
\markup {
  \fill-line {
    \hspace #1
    \column {
      \line { 1. }
      \line { O Haupt voll Blut und Wunden, }
      \line { voll Schmerz und voller Hohn, }
      \line { o Haupt, zum Spott gebunden }
      \line { mit einer Dornenkron, }
      \line { o Haupt, sonst schön gezieret }
      \line { mit höchster Ehr und Zier, }
      \line { jetzt aber hoch schimpfieret: }
      \line { gegrüßet seist du mir! }
    }
    \hspace #2
    \column {
      \line { 3. }
      \line { Die Farbe deiner Wangen, }
      \line { der roten Lippen Pracht }
      \line { ist hin und ganz vergangen; }
      \line { des blassen Todes Macht }
      \line { hat alles hingenommen, }
      \line { hat alles hingerafft, }
      \line { und daher bist du kommen }
      \line { von deines Leibes Kraft. }
    }
    \hspace #1
  }
}