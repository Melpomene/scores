\version "2.20.0"
\language "deutsch"

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}
    
\header {
  title = "Maria durch ein Dornenwald ging"
  subtitle = "Volkslied, vor 1850"
  poet = "Volksweise, vor 1850"
  composer = "Satz: Heinrich Kaminski (1886–1946)"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
    \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key a \minor
  \time 4/4
}

melismataOff = \set ignoreMelismata = ##t 
melismataOn = \unset ignoreMelismata 
pdolce = \markup { \dynamic p \italic { dolce } }

rightbrace = \markup {
  \hspace #2 { \column { \vspace #-0.25 \right-brace #35 } }
}

sopranoIWordsI = \lyricmode {
  \set stanza = "1. "
  Ma -- ri -- a durch ein Dor -- nen -- wald ging,
  Ky -- rie e -- lei -- son. __
  Ma -- ri -- a durch ein Dor -- wald ging,
  der hatt in siebn Jahr kein Laub ge -- tra -- gen.
}
sopranoIWordsII = \lyricmode {
  \set stanza = "2. "
  Was trug Ma -- ri -- a \melismataOff un -- ter ih -- rem \melismataOn Her -- zen?
  Ky -- rie e -- lei -- son. __
  Ein klei -- nes Kind -- lein oh -- ne \melismataOff Schmer -- zen, \melismataOn
  das trug Ma -- \melismataOff ri -- a \melismataOn un -- ter ih -- rem Her -- zen.
  Je -- sus und Ma -- ri -- a.
}
sopranoIWordsIII = \lyricmode {
  \set stanza = "3. "
  \melismataOff
  Da ha -- ben die Dor -- nen Ro -- _ sen ge -- tra -- _ gen, \melismataOn
  Ky -- rie e -- lei -- son. __
  \melismataOff Als das \melismataOn Kin -- lein durch den Wald ge -- tragn, 
  da \melismataOff ha -- ben \melismataOn die Dor -- nen Ro -- sen ge -- tra -- gen.
}

sopranoIIWordsI = \lyricmode {
  \set stanza = "1. "
  Ma -- ri -- a durch ein Dor -- nen -- wald __ ging, __
  Ky -- rie e -- lei -- son. __
  Ma -- ri -- a durch ein Dor -- wald ging,
  der hatt in siebn Jahr kein Laub _ __ ge -- tra -- gen.
}
sopranoIIWordsII = \lyricmode {
  \set stanza = "2. "
  \melismataOff
  Was trug _ Ma -- ri -- a un -- ter ih -- _ rem Her -- zen? _ 
  Ky -- rie e -- lei -- _ son. __ _
  Ein klei -- _ nes Kind -- lein oh -- ne Schmer -- zen,
  das trug _ Ma -- ri -- a un -- ter ih -- _ _ rem Her -- zen.
  Je -- sus und Ma -- ri -- a.
  \melismataOn
}
sopranoIIWordsIII = \lyricmode {
  \set stanza = "3. "
  \melismataOff 
  Da ha -- ben die Dor -- nen Ro -- _ _ sen ge -- tra -- gen, _ 
  Ky -- rie e -- lei -- _ son. __ 
  Als das Kin -- lein durch _ den Wald ge -- tra -- gn, 
  da ha -- ben die Dor -- _ _ nen Ro -- _ sen ge -- tra -- gen.
  \melismataOn
}

altoWordsI = \lyricmode {
  \set stanza = "1. "
  Ma -- ri -- a durch ein Dor -- nen -- wald ging,
  Ky -- ri -- e e -- lei -- son. __
  Ma -- ri -- a durch ein Dor -- wald ging,
  der hatt in siebn Jahr kein Laub _ __ ge -- tra -- gen.
}
altoWordsII = \lyricmode {
  \set stanza = "2. "
  Was trug Ma -- ri -- a un -- term Her -- zen?
  Ky -- ri -- e e -- lei -- son. __
  Ein klei -- nes Kind -- lein oh -- ne \melismataOff Schmer -- zen, \melismataOn
  das trug \melismataOff Ma -- ri -- a \melismataOn un -- ter ih -- _ rem Her -- zen.
  Je -- sus und Ma -- ri -- a. __
}
altoWordsIII = \lyricmode {
  \set stanza = "3. "
  \melismataOff
  Da ha -- ben die Dor -- nen Ro -- sen ge -- tra -- gen, _ 
  Ky -- _ ri -- e e -- lei -- son. __
  Als das Kin -- _ lein durch den Wald ge -- tra -- gn, 
  da ha -- ben die Dor -- _ _  nen Ro -- _ sen ge -- tra -- gen.
}

tenorWords = \lyricmode {
  Ky -- rie e -- lei -- son __
  e -- lei -- son,
  Ky -- ri -- e  e -- lei -- son,
  Je -- sus und Ma -- ri -- a.
}
bassIWords = \lyricmode {
  Ky -- rie e -- lei -- son __
  Je -- sus und Ma -- ri -- a.
}
bassIIWords = \lyricmode {
  Ky -- rie e -- lei -- son
  Je -- sus und Ma -- ri -- a.
}


sopranoIMusic = \relative c' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  e4^\pdolce ^\markup { \large Sehr ruhig }
  \slurDashed a4( a8) h8 c4 e | \slurDashed c8( c)  \slurDashed h8( a) h[ gis] e4 |
  %% Breit
  c'4^\p ^\markup { \large Breit } c8 \< c d2 e2. \>
  %% Sehr ruhig
  \slurDashed c8 \! ^\pdolce ^\markup {\large Sehr ruhig } ( d ) |
  e4. d8 e4 f8[ e ] | d4. c8 d4 \slurDashed e8( d) | 
  \slurDashed c4( c8) h8 \slurDashed c8( \( c) d8 \) c | \slurDashed h4( h8) a h [ gis ] e4 |
  %% Breit
  a4.^\markup { \large Breit } h8 c4 e | c8[( h )] a2 \bar "|."
}

sopranoIIMusic = \relative c' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  e4^\pdolce
  \slurDashed a4( a8) h8 c4 h | a8 g f4( f8) e (h'8[ e,~ ] | 
  << { e8^\p ) a4 a a8 g[ f] } { s4 s \< s4 s } >> | e2.~ \>  e8 \!
  %% Sehr ruhig
  h'^\pdolce | c4. g4 \slurDashed c8( f,) a | \slurDashed h( a) g4 f8 g4. |
  \slurDashed a4( a8) h8 g8 \([ e] f \)  e | d \( [ c ] d \) f e4 e |
  e4. g8 a4 g | e e2
}

altoMusic = \relative c' {
  \clef "treble"
  \partial 4
  \autoBeamOff
  e4^\pdolce | a \slurDashed e8 ( e) a4 g | f8 e \slurDashed d4( d8) e8~ e
  d8 ^\p( c ) e f \< e d2 | c2. \> ( c8 ) \!
  g'8^\pdolce | g2( g8 ) c, a c | f4 e d ( c8 ) d |
  \slurDashed e4( e8) d8 c8 [ \( e ] a \) g | f [ \( e ] d \) c h4. h8 |
  c4. h8 a4 e' | a8 e( e2 )
}

tenorMusic = \relative c' {
  \clef "G_8"
  \partial 4
  \autoBeamOff
  r4 | R1*2 |
  a4^\p c \< ( c8 ) c h4 | g4. \> r8 \! r8 e8^\p\(( a[ g) ] c[( d ] e4) \) c r | 
  r2 r8 h^\p( c) h | a( [ g) ] f4 e( d~ | d )  e2 
  gis4 ( | e g! \tuplet 3/2 { c,8 ) c c~ } c8 c' | 
  e4 cis?2^\markup { \halign #3 (3. Str. \raise #1 { \sharp } ) }
}

bassIMusic = \relative c' {
  \clef "bass"
  \partial 4
  \autoBeamOff
  r4 | R1*2 | a4^\p f ( \< f8 ) d g4 | c,2. \> r4 \! | 
  R1*3 | r2 r4 r8 e8^\p( a,4 ) d f g | a8[ gis ] a2
}

bassIIMusic = \relative c {
  \clef "bass"
  \partial 4
  \autoBeamOff
  r4 | R1*3 | r8 c^\p \( c c g' e \) r4 | R1*4 |
  a,4^\p g! f c'~ \( | \tuplet 3/2 { c8 [ d ] \) e } <e a,>2
}
  

\new ChoirStaff <<
  \new ChoirStaff \with { instrumentName = #"Sopran " } <<
    \new Staff = "sopranosI"
    <<
      \new Voice = "sopranosI" {
	\global
	\sopranoIMusic
      }
    >>
     \new Lyrics \lyricsto "sopranosI" {
    \sopranoIWordsI
  }
  \new Lyrics \lyricsto "sopranosI" {
    \sopranoIWordsII
  }
  \new Lyrics \lyricsto "sopranosI" {
    \sopranoIWordsIII
  }
 \new Staff = "sopranosII"
    <<
      \new Voice = "sopranosII" {
	\global
	\sopranoIIMusic
      }
    >>
  \new Lyrics \lyricsto "sopranosII" {
    \sopranoIIWordsI
  }
  \new Lyrics \lyricsto "sopranosII" {
    \sopranoIIWordsII
  }
  \new Lyrics \lyricsto "sopranosII" {
    \sopranoIIWordsIII
  }
  >>
  \new Staff = "altos" \with { instrumentName = #"Alt " }
  <<
    \new Voice = "altos" {
      \global
      \altoMusic
    }
  >>
  \new Lyrics \lyricsto "altos" {
    \altoWordsI
  }
  \new Lyrics \lyricsto "altos" {
    \altoWordsII
  }
  \new Lyrics \lyricsto "altos" {
    \altoWordsIII
  }
  \new Staff = "tenors" \with { instrumentName = #"Tenor " }
  <<
    \new Voice = "tenors" {
      \global
      \tenorMusic
    }
  >>
  \new Lyrics \lyricsto "tenors" {
    \tenorWords
  }
  \new ChoirStaff \with { instrumentName = #"Bass " } <<
    \new Staff = "bassesI"
    <<
      \new Voice = "bassesI" {
	\global
	\bassIMusic
      }
    >>
  \new Lyrics \lyricsto "bassesI" {
    \bassIWords
  }
    \new Staff = "bassesII"
    <<
      \new Voice = "bassesII" {
	\global
	\bassIIMusic
      }
    >>
  \new Lyrics \lyricsto "bassesII" {
    \bassIIWords
  }
  >>
>>  % end ChoirStaff