\version "2.20.0"
\language "deutsch"

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}
    
\header {
  title = "Freu dich, Erd und Sternenzelt"
  poet = "Text: Leitmeritz 1844"
  composer = "Carl Riedel (1827–1888)"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
    \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key g \major
  \time 2/4
  \autoBeamOff
}

refrain = \lyricmode {
  Uns zum Heil er -- ko -- ren,
  ward er heut ge -- bo -- ren,
  ward uns heut ge -- bo -- ren.
}
sopranoRefrain = \lyricmode {
  \repeat unfold 15 { ""2 }  heu4 -- te  uns8 ge -- bo4 -- ren.2
}
wordsI = \lyricmode {
  \set stanza = "1. "
  Freu dich, Erd und Ster -- nen -- zelt, \repeat unfold 4 ""
  Got -- tes Sohn kam in die Welt, 
}
wordsII = \lyricmode {
  \set stanza = "2. "
  Seht, der schön -- sten Ro -- se Flor, Hal -- le -- lu -- ja!
  sprießt aus Jes -- ses Zweig em -- por, Hal -- le -- lu -- ja!
  \refrain
}
wordsIII = \lyricmode {
  \set stanza = "3. "
  Ehr sei Gott im höch -- sten Thron, \repeat unfold 4 ""
  der uns schenkt sein lie -- ben Sohn, 
}

sopranoMusic = \relative c'' {
  \tempo "Sanft und anmutig"
  \dynamicUp
  \clef "treble"
  g8\mf \< a h c \> | d d d4 | c8\f d e4 \> | d4.\mf h8\rest
  d8 \< d c \! c | h h a4 | h8\p c a4 | g4. h8\rest
  h8\p \< a h c | d4.\mf ( \> c8) h4\p h4\rest |
  h8\mf \< a h c | d4.\f ( \> c8 )
  | h4\p a4\rest |
  R2 | g4 \< e'| d8\f \> h a4 | g2\p \bar "|."
}

altoMusic = \relative c'' {
  \clef "treble"
  g8 d d g fis g fis4 | e8 g g[ fis] g4. s8 |
  g8 g e e | fis fis a4 | g8 g g[ fis ] g4. s8 |
  d8 d g g | d4( e8[ fis] ) | g4 s4 |
  g8 a fis a | h4( a ) | g s4 |
  d4 g^\espressivo | g g | fis8 g a[ fis] d2
}

tenorMusic = \relative c' {
  \clef "G_8"
  d8 a g g | a h a4 | a8 h c4 | h4. s8 |
  g'8 d e a, | d d e4 | d8 e d[ a] | h4. s8 |
  g8 a g e | fis4 ( a8[ d ] ) | d4 s4 |
  e8 e dis e | g4( e) | e s |
  h4 e | d c | a8 g e'8[ d16 d] | h2
}

bassMusic = \relative c' {
  \clef "bass"
  h8 fis g e | d g d4 | a'8 g c, 4 | g'4. a8\rest | 
  h8 h, c c | d h' c4 | g8\p c, d4 g,4. d'8\rest |
  g8\p \< fis e c | h4\mf( a) \> | g\p d'4\rest |
  e8\mf \< c' h a | g4\f \> ( a) | e\p a4\rest |
  g4 c,_\espressivo h \< c | d8\f e \> c[ d] g,2\p
}


\new ChoirStaff <<
  \new Lyrics { \sopranoRefrain }
  \new Staff = "females"
  <<
    \new Voice = "sopranos" {
      \global
      \voiceOne
      \sopranoMusic
    }
    \new Voice = "altos" {
      \global
      \voiceTwo
      \altoMusic
    }
  >>
  \new Lyrics \lyricsto "altos" {
    \wordsI
  }
  \new Lyrics \lyricsto "altos" {
    \wordsII
  }
  \new Lyrics \lyricsto "altos" {
    \wordsIII
  }
  \new Staff = "males" 
  <<
    \new Voice = "tenors" {
      \global
      \voiceOne
      \tenorMusic
    }
    \new Voice = "basses" {
      \global
      \voiceTwo
      \bassMusic
    }
  >>
>>