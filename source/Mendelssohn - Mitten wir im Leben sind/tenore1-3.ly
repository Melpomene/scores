\new Staff \with { instrumentName = "Tenor I" }
      << 
        \new Voice = "tenore1" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_three_tenore_one
        }
        \new Lyrics \lyricsto "tenore1" {
          \mitten_wir_three_text_m
          Hei -- li -- ger Her -- re Gott, star -- ker Gott,
          barm -- her -- zi -- ger Hei -- land, du e -- wi -- ger Gott, 
          laß uns nicht ent -- fal -- len -- von des __ rech -- ten __ Glau -- bens Trost.
          Ky -- ri -- e e -- lei -- son, __
          Ky -- ri -- e e -- lei -- son, __
          e -- le -- i -- son.
        }
      >>
