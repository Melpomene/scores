\new Staff \with { instrumentName = "Alt I" }
      <<
        \new Voice = "alto1" { 
          \mitten_wir_meta
          \mitten_wir_one_alto_one
        }
        \new Lyrics \lyricsto "alto1" {
          \mitten_wir_one_text_f
          \mitten_wir_text_h __
          du e -- wi -- ger Gott, du e -- wi -- ger Gott, __ du __ ew' -- ger Gott,
          in des bit -- tern To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, 
          Ky -- ri -- e, e -- le -- i -- son, __  e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, e -- le -- i -- son.
        }
      >>
