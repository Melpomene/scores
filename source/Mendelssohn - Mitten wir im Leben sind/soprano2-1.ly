\new Staff \with { instrumentName = "Sopran II" }
      <<
        \new Voice = "soprano2" { 
          \mitten_wir_meta
          \mitten_wir_one_soprano_two
        }
        \new Lyrics \lyricsto "soprano2" {
          \mitten_wir_one_text_f
          \mitten_wir_text_h
          hei -- li -- ger Her -- re Gott, hei -- li -- ger star -- ker Gott,
          hei -- li -- ger, hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
          du  e -- wi -- ger Gott,
          laß uns nicht ver -- sin -- ken in  To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e, Ky -- ri -- e e -- le -- i -- son, e -- le _ -- i -- son.
        }
      >>
