mitten_wir_one_basso_two = {
  \relative c' {
          g2^\f g |
          c2 g |
          g2 g |
          g,1^\fermata |
          g'2 c, |
          f,2 as |
          g1 |
          c1^\fermata |

          \barNumberCheck 9
          g'2 g |
          c,2 g |
          c2 f |
          b,!1^\fermata |
          b4 ( as ) g2 |
          f2 f' |
          f2 ( f, ) |
          g2^\fermata r2 |
          R1*3

          \barNumberCheck 20
          r2^\fermata g'2^\p |
          e2 c |
          f, c' |
          des2. des4 |
          c1^\fermata |
          as2 b |
          c2 c4( b ) |
          as2.^> as4 |
          g1^\fermata
        }
  \relative c {
    %% Vivace
          \barNumberCheck 29
          c2^\f c4 c
          c2. c4 |
          c'1^\fermata |
          c2 c4 c |
          f,1~ |
          f2 f |
          c1~ |
          c2 r2 |
          c'4. c8 c4 c |
          f,2 f4 g |
          g,1 |
          c1~ |
          c2 r2 |

          \barNumberCheck 42
          as'!1^\f|
          b2. as4 |
          g1 | b2. b4
          as2 ( g ) | 
          f2 c |
          as'2 ( g ) |
          f2 ( c ) |
          as1 |
          g1 |
          c1~ |
          c1~ |
          c1 \breathe |
          c1 |
          as'2 ( g ) |
          f2 e |
          f2 f4 c |
          as2 f |
        }

  \relative c {
    \barNumberCheck 60
    c4^\ff ( g' c b |
    as4 g as f ) |
    b4 ( as ) g ( f ) |
    e2. e4 |
    f4 ( g ) as ( b ) |
    e,2 r4 e |
    as4 ( g as f |
    h2. ) h4 |
    c2 r2 |

    \barNumberCheck 69
    f,2^\ff  f |
    des2. des4 |
    c4 ( g ) as ( b ) |
    as2 r4 f'4 |
    b2 b4 ( c ) |
    des2 r4 f,, |
    b2 b4 ( c ) |
    des2. c4 |
    h1 ( |
    b!1 |
    a1 |
    as!1 |
    g1~ |
    g1~ |
    g2 c |
    h2 ) c |
    g1~ |
    g1~ |
    g1 \fermata \bar "||"
  }
}

mitten_wir_two_basso_two = {
  \relative c {
    g'2^\f g 
    | % 89
    c g 
    | % 90
    c, c 
    | % 91
    g1 \fermata
    | % 92
    g'2 c, 
    | % 93
    f, f' 
    | % 94
    g1 
    | % 95
    c, \fermata 
    | % 96
    g'2 g 
    | % 97
    c, g 
    | % 98
    c f, 
    | % 99
    b!1 \fermata 
    | % 100
    b4 ( as ) g2 
    | % 101
    f as4 ( b )
    | % 102
    c2 ( f, )
    | % 103
    g2 \fermata r2 |
    R1*3
    \barNumberCheck 107
    r2 \fermata  g'2^\p |
    e c f, c' des2. des4 c1 \fermata 
    as2 b 
    c c4 ( b )
    as2. as4 
    g1 \fermata
    \bar "||"
  }
  \relative c {
    \barNumberCheck 116
    c2^\f c4 c 
    c2. c4 
    c'1^\fermata
    c2 c4 c 
    f,1~ |
    f2 f2 |
    c'1~ |
    c2 r2 |
    c4. c8 c4 c f,2 
    f4 g | 
    g,1 |
    c1~ c1~ c1~ |
    c4 \breathe c'4 c b8 c |
    as2 g |
    as g f ( e f2. ) c4 |
    as2 f |
    \barNumberCheck 136
    c'4^\ff ( g' c b 
    | % 137
    as g as f )
    | % 138
    b ( as ) g ( f )
    | % 139
    e2. e4 
    | % 140
    f ( g ) as ( b )
    | % 141
    e,2 r4 e 
    | % 142
    f2. f4 
    | % 143
    ges2. ( f4 
    | % 144
    es2 ) es 
    | % 145
    f2. f4 
    | % 146
    b,1 
    | % 147
    r1
    \barNumberCheck 148
    f'2^\ff f 
    | % 149
    des2. b4 
    | % 150
    e1 (
    | % 151
    es! 
    | % 152
    d!2 ) c 
    | % 153
    h1 
    | % 154
    b!
    | % 155
    a (
    | % 156
    as! 
    | % 157
    g1~ g1~ g2 c2 h ) c |
    g1~ g~ g^\fermata
    \bar "||"
  }
}

mitten_wir_three_basso_two = {
  \set Score.currentBarNumber = 164
  \relative c' {
  g2^\f  g 
  | % 165
  c g 
  | % 166
  c, ( g 
  | % 167
  c ) c 
  | % 168
  g1^\fermata
  | % 169
  g'2 c, 
  | % 170
  f, as 
  | % 171
  g1 
  | % 172
  c^\fermata
  \barNumberCheck 173
  g'2^\mf g 
  | % 174
  c g 
  | % 175
  c, c 
  | % 176
  d1^\fermata
  | % 177
  g2 \> c, 
  | % 178
  as f 
  | % 179
  g4 ( as b2 )|
  \after 4 \! es^\fermata r2 |
}
  \relative c' {
    \barNumberCheck 181
    r2
    ^\markup {
      \epsfile #X #5 #"Mendelssohn - Mitten wir im Leben sind/tacet.eps"
    }
    g2^\f |
    g2. g4 |
  c,1 |
  g2^\fermata g'4 (f )
  | % 185
  e2 c 
  | % 186
  f c 
  | % 187
  des2. des4 
  | % 188
  c1^\fermata 
  | % 189
  as2 b 
  | % 190
  c c4 ( b )
  | % 191
  as2 as 
  | % 192
  g1^\fermata
}
  \relative c {
  \barNumberCheck 193
  c2^\p c4 c 
  | % 194
  c1~ |
  c2 c2 |
  c1 f,1~ \< |
  f2 \> f2 \!
  c'1^\fermata
  c1^\pp 
  | % 201
  f2 c4 c 
  | % 202
  g'1 
  | % 203
  c,2 r2 |
  r2 c2^\p |
  f, c'|
  f^\cresc \breathe es! |
  as g4.^\f g8 |
  c2 g |
  c, c^\dim |
  f,1 f 
  | % 212
  g^\p 
  | % 213
  g 
  | % 214
  c^\fermata 
  | % 215
  c'2.^\p b!4 
  | % 216
  as2 g 
  | % 217
  f1 
  | % 218
  c2 r2 
  | % 219
  c'2. b4 
  | % 220
  as2 g 
  | % 221
  f1 
  | % 222
  c1~ |
  c2 r2
  R1*2
  \barNumberCheck 226
  f,1\pp^>
  | % 227
  f^> 
  | % 228
  f^>
  | % 229
  c'^\espressivo
  \fermata 
  \bar "|."
}
}