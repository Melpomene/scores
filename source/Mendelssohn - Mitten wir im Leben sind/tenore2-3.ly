\new Staff \with { instrumentName = "Tenor II" }
      << 
        \new Voice = "tenore2" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_three_tenore_two
        }
        \new Lyrics \lyricsto "tenore2" {
          \mitten_wir_three_text_m
          Hei -- li -- ger Her -- re Gott, star -- ker Gott,
          barm -- her -- zi -- ger Hei -- land, du e -- wi -- ger Gott, 
          laß uns nicht ent -- fal -- len -- von des rech -- ten Glau -- bens __ Trost.
          Ky -- ri -- e e -- lei -- son,
          Ky -- ri -- e e -- lei -- son, __
          e -- le -- i -- son.
        }
      >>
