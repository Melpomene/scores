\new Staff \with { instrumentName = "Bass I" }
      <<
        \new Voice = "basso1" { 
          \mitten_wir_meta
          \clef "bass"
          \mitten_wir_three_basso_one
        }
        \new Lyrics \lyricsto "basso1" {
          \mitten_wir_three_text_m
          Hei -- li -- ger Her -- re Gott, star -- ker Gott,
          barm -- her -- zi -- ger Hei -- land, du ew' -- ger Gott, 
          laß uns nicht ent -- fal -- len -- von des rech -- ten Glau -- bens Trost.
          Ky -- ri -- e e -- lei -- son,
          Ky -- ri -- e e -- lei -- son, __
          e -- le -- i -- son.
        }
      >>
