      \new Staff \with { instrumentName = "Sopran I" }
      <<
        \textMark \markup { \bold "Tempo I" }
        \new Voice = "soprano1" { 
          \mitten_wir_meta
          \mitten_wir_two_soprano_one
        }
        \new Lyrics \lyricsto "soprano1" {
          \mitten_wir_two_text_f __
          \mitten_wir_text_h
          du e -- wi -- ger Gott,
          laß uns nicht ver -- za -- gen vor der tie -- fen Höl -- len Glut.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, Ky -- rie -- e __ e -- le -- i -- son, e -- le _ -- i -- son.
        }
      >>
