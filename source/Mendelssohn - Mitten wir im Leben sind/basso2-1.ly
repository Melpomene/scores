\new Staff \with { instrumentName = "Bass II" }
      <<
        \new Voice = "basso2" { 
          \mitten_wir_meta
          \clef "bass"
          \mitten_wir_one_basso_two
        }
        \new Lyrics \lyricsto "basso2" {
          \mitten_wir_one_text_m
          \mitten_wir_text_h __
          du e -- wi -- ger Gott, du e -- wi -- ger Gott, __ du __ ew' -- ger Gott, __
          
          laß uns nicht ver -- sin -- ken in To -- des Not. __
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, e -- le -- i -- son,
          e -- le -- i -- son. __
        }
      >>
