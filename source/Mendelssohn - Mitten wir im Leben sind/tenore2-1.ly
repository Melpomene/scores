\new Staff \with { instrumentName = "Tenor II" }
      << 
        \new Voice = "tenore2" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_one_tenore_two
        }
        \new Lyrics \lyricsto "tenore2" {
          \mitten_wir_one_text_m
          \mitten_wir_text_h
          hei -- li -- ger Her -- re Gott, hei -- li -- ger star -- ker Gott,
          hei -- li -- ger hei -- li -- ger barm -- her -- zi -- ger -- Hei -- land, 
          du ew' -- ger Gott,
          laß uns nicht ver -- sin -- ken in des bit -- tern To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, __ e -- le -- i -- son,
          e -- le -- i -- son, e -- le -- i -- son.
        }
      >>
