      \new Staff \with { instrumentName = "Bass II" }
      <<
        \new Voice = "basso2" { 
          \mitten_wir_meta
          \clef "bass"
          \mitten_wir_two_basso_two
        }
        \new Lyrics \lyricsto "basso2" {
          \mitten_wir_two_text_m
          \mitten_wir_text_h __
          laß uns nicht ver -- za -- gen vor der tie -- fen Höl -- len Glut. __
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, __ e -- le -- i -- son, 
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son. __
        }
      >>
