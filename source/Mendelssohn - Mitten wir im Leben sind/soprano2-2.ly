      \new Staff \with { instrumentName = "Sopran II" }
      <<
        \new Voice = "soprano2" { 
          \mitten_wir_meta
          \mitten_wir_two_soprano_two
        }
        \new Lyrics \lyricsto "soprano2" {
          \mitten_wir_two_text_f
          \mitten_wir_text_h
          du -- e -- wi -- ger Gott,
          laß uns nicht ver -- za -- gen.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son.
          Ky -- ri -- e __ e -- le -- i -- son, e -- le _ -- i -- son,
        }
      >>
