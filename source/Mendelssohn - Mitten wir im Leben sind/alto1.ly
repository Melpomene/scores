mitten_wir_one_alto_one = {
  \repeat unfold 3 { R1*3 r1\fermata }
  R1*3
  \relative c'' {
    \barNumberCheck 16
    r2\fermata g2^\p |
    g2. g4 |
    g2 g |
    g2 ( c ) | h2\fermata r2
    \repeat unfold 2 { R1*3 r1\fermata } \bar "||"
    
    \barNumberCheck 29
    c2^\f c4 c |
    c2. g4 | g1^\fermata |
    c2 c4 c |
    c1~ | c2 f, |
    g1~ |
    g2 r2 |
    c4. c8 c4 g |
    as2 as4 c |
    c2 ( h ) |
    c1~ | c2 r2 |
    
    \barNumberCheck 42
    r2 c^\f |
    c2 ( b ) |
    b2 g |
    f2. f4
    
    \barNumberCheck 46
    f2 ( e ) |
    f2 g |
    f 2 ( e ) |
    f2 ( g ) |
    f1 | f1 | e1 |
  }
  \relative c'' {
    \barNumberCheck 53
    R1*5
    des4^\f des des c |
    c2 f, |
    g2 r2 |
  }
  
  \relative c''{
    \barNumberCheck 61
    f2^\ff f |
    des2. des4 |
    c4 ( g ) as ( b ) |
    as2 r2 |
    c2 c |
    as2. as4 |
    g4 ( d! ) e ( f ) |
    e4 ( g as b |
    as b ) c ( as ) |
    f2 (e4 f |
    g4 des' c b |
    c4 ) as f r4 |
  }
  \relative c'' {
    \barNumberCheck 73
    des2 des |
    b2. c4 |
    des2 des |
    b2. c4 |
    d!2. ( h4 |
    g4 b! as g~ |
    g4 c, f e |
    f2 fis |
    g2 ) g |
    g2 \breathe g |
    h2 ( \melisma c |
    d2 c )  |
    c1 ( |
    h2  ) \melismaEnd a |
    h1\fermata \bar "||"
  }
}

mitten_wir_two_alto_one = {
  \set Score.currentBarNumber = 88
  \repeat unfold 3 { R1*3 r1^\fermata }
  R1*3
  \relative c'' {
    r2^\fermata g2^\p |
    g2. g4 g2 g
    g2 ( c )
    | % 107
    h^\fermata r2 |
    \repeat unfold 2 { R1*3 r1^\fermata }
    \bar "||" |
    \barNumberCheck 116
    c2^\f c4 c 
    | % 117
    c2. g4 
    | % 118
    g1^\fermata
    | % 119
    c2 c4 c 
    | % 120
    c1~ c2 as2 g1~ g2 r2
    c4. c8 
    c4 g as2 
    as4 c |
    c2 ( h ) |
    c   e, |
    f (  g  ) |
    as  b |
    as r2 |
    \barNumberCheck 131
    R1*3
    des4^\f des des c 
    | % 135
    c2 f, 
    | % 136
    g r2 |
    \barNumberCheck 137
    f'^\ff f 
    | % 138
    des2. des4 
    | % 139
    c ( g ) as ( b )
    | % 140
    as2 r4 des 
    | % 141
    c ( g ) as ( b )
    | % 142
    as2 r2 
    | % 143
    b2^\ff  b 
    | % 144
    ges2. ges4 
    | % 145
    f ( c ) des ( es )
    | % 146
    des (as' g ) f 
    | % 147
    e ( b' as g 
    | % 148
    c b c as 
    | % 149
    f2 e4 f 
    | % 150
    g2 c2~ |
    c2 ) f,2
    | % 152
    f2 r4 f
    g ( d 
    e f e b' 
    as g4~ |
    g4 c,4 f e |
    f2 fis g ) g g g |
    h (
    c d 
    c2~ c1 h2 ) a 
    h1^\fermata
  }
}

mitten_wir_three_alto_one = {
 \set Score.currentBarNumber = 164
  \relative c'' {
    r1
    | % 165
    es,2^\f g 
    | % 166
    g g 
    | % 167
    g4 ( f ) es2 
    | % 168
    g1^\fermata
    | % 169
    g2 g 
    | % 170
    f f 
    | % 171
    es4 ( f g2 )
    | % 172
    g1^\fermata
    | % 173
    g2^\mf g 
    | % 174
    g g 
    | % 175
    g4 ( b!2 ) a8 [ ( g ] )
    | % 176
    fis1^\fermata 
    | % 177
    g2 \> es 
    | % 178
    c c4 ( d )
    | % 179
    es2. ( d4 )
    | % 180
    \after 4 \! es2^\fermata g^\f
    | % 181
    g2. g4 
    | % 182
    g2 g 
    | % 183
    g ( c )
    | % 184
    h^\fermata d, 
    | % 185
    c e 
    | % 186
    f e 
    | % 187
    f b, 
    | % 188
    e1^\fermata 
    | % 189
    f2 g 
    | % 190
    f e 
    | % 191
    f c 
    | % 192
    h1^\fermata
  }
  \relative c' {
    \barNumberCheck 193
    c2^\p c4 c 
    | % 194
    c2. d4 
    | % 195
    e1 
    | % 196
    c2 c4 c 
    | % 197
    f1~ \< |
    f2 \> c4 ( \! d ) |
    e1^\fermata |
  }
  \relative c' {
    \barNumberCheck 200
    c4.^\pp c8 c4 c 
    | % 201
    c2 c4 c 
    | % 202
    c2 ( h )
    | % 203
    c c^\p 
    | % 204
    c1~ |
    c2 e2 |
    f4^\cresc ( as2 ) \breathe g4 |
    as4 c2^\f h4 |
    c2 d |
    c g^\dim 
    | % 210
    f1~ |
    f2 as!2 |
    es^\p ( g ) |
    g1 |
    g^\fermata 
  }
  \relative c' {
    \barNumberCheck 215
    R1*2 |
    f2. f4 
    | % 218
    e1~ |
    e2 r2 |
    r1 |
    f2. f4 
    | % 222
    e2 r2 
    | % 223
    c2.^\pp c4 
    | % 224
    c2 c 
    | % 225
    c ( f2~ ) |
    f1~ f~ f 
    e1^\espressivo
    \fermata
 }  
}
