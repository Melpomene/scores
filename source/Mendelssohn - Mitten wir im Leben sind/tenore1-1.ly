\new Staff \with { instrumentName = "Tenor I" }
      << 
        \new Voice = "tenore1" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_one_tenore_one
        }
        \new Lyrics \lyricsto "tenore1" {
          \mitten_wir_one_text_m
          \mitten_wir_text_h __
          du ew' -- ger Gott, du e -- wi -- ger Gott, __ du ew' -- ger Gott,
          laß uns nicht ver -- sin -- ken in des bit -- tern To -- des Not, __ in des To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e, Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son.
        }
      >>
