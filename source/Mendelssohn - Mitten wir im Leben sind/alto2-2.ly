      \new Staff \with { instrumentName = "Alt II" }
      <<
        \new Voice = "alto2" { 
          \mitten_wir_meta
          \mitten_wir_two_alto_two
        }
        \new Lyrics \lyricsto "alto2" {
          \mitten_wir_two_text_f
          \mitten_wir_text_h
          du -- e -- wi -- ger Gott,
          vor der tie -- fen Höl -- len Glut, vor der Höl -- len Glut.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son.
          Ky -- ri -- e e -- le -- i -- son, __ e -- le -- i -- son,
          e -- le -- i -- son, e -- le -- i -- son.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son. __
        }
      >>
