\new Staff \with { instrumentName = "Alt I" }
      <<
        \new Voice = "alto1" { 
          \mitten_wir_meta
          \mitten_wir_three_alto_one
        }
        \new Lyrics \lyricsto "alto1" {
          \mitten_wir_three_text_a
	  Hei -- li -- ger Her -- re Gott,  hei -- li -- ger star -- ker Gott,
	  Hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
          du ew' -- ger Gott,
          laß uns nicht ent -- fal -- len von des rech -- ten Glau -- bens Trost.
          Ky -- ri -- e, __ Ky -- ri -- e,
          Ky -- ri -- e e -- lei -- son.
        }
      >>
