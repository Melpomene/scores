\new Staff \with { instrumentName = "Sopran I" }
      <<
        \textMark \markup { \bold "Tempo I" }
        \new Voice = "soprano1" { 
          \mitten_wir_meta
          \mitten_wir_three_soprano_one
        }
        \new Lyrics \lyricsto "soprano1" {
          Mit -- ten in der Höl -- len Angst un -- re Sünd uns trei -- ben.
          Wo solln wir denn flie -- hen hin, da wir mö -- gen blei -- ben?
          Zu dir, Herr Christ,  al -- lei -- ne. __
          Ver -- gos -- sen ist dein teu -- res Blut,
          das gnug für die Sün -- de tut.
	  Hei -- li -- ger Her -- re Gott,  hei -- li -- ger star -- ker Gott,
	  Hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
          du e -- wi -- ger Gott,
          laß uns nicht ent -- fal -- len von des rech -- ten __ Glau -- bens Trost.
          Ky -- ri -- e, __ Ky -- ri -- e,
          Ky -- ri -- e e -- lei _ -- son.
        }
      >>
