mitten_wir_one_basso_one = {
  \relative c' {
          g2^\f g |
          c2 g |
          g2 g |
          g1\fermata |
          g2 g |
          as2 f |
          g2. ( f4 ) |
          es1\fermata

          \barNumberCheck 9
          g2 g |
          es2 g |
          g2 a |
          f1\fermata |
          f2 g |
          as!2 f |
          f2 ( d4 c ) |
          h2\fermata r2 |
          R1*3

          \barNumberCheck 20
          r2\fermata g'^\p |
          e2 c |
          f,2 c' |
          des2. des4 |
          c1\fermata |
          as2 des |
          c2 c |
          c2.^> c4 |
          d!1\fermata \bar "||"
        }
  \relative c' {
          \barNumberCheck 29
          c2^\f c4 c |
          c2. c,4 |
          c1\fermata |
          c'2 c4 c |
          c1~ |
          c2 f,2 |
          c'1~ |
          c2 r2 |
          c4. c8 c4 c
          f,2 f4 c |
          g'1 |
          c,1 |
        }
  \relative c' {
          \barNumberCheck 41
          c2^\f c4 c |
          c4. c8 c2 |
          d2 d4 d |
          es4. es,8 es2 |
          r1
        }
  \relative c' {
          \barNumberCheck 46
          c4.^\f c8 c4 c |
          c2 c4 c |
          c1 |
          c2 c |
          c1 | g1 |
          c,4 \breathe c'^\f c b8 c |
          as2 g |
          f2 e |
          f2. ( e4 ) |
          c1 |
          c1~ |
          c1~ |
          c2 c |

          \barNumberCheck 60
          c4^\ff ( g' c b |
          as g as f ) |
          b4 ( as ) g ( f ) |
          e2. e4 |
          f4 ( g ) as ( b ) |
          e,2 r4 e4 |
          as4 ( g as f |
          h2. ) h4 |
          c2 r2 |
        }
  \relative c' {
    \barNumberCheck 69
    f2^\ff f |
    des2. des4 |
    c4 ( g ) as ( b )

    \barNumberCheck 72
    as2 r4 f |
    b2 b4 ( c ) |
    des2 r4 f, |
    b4 ( as )  g( as )
    b2 des,4 ( c ) |
    h!1 ( |
    b!1 |
    a!1 |
    as!1 |
    g1~ |
    g1~ |
    g2 c |
    h2 ) c |
    g'1~ |
    g1~ |
    g1\fermata \bar "||"
  }
}

mitten_wir_two_basso_one = {
  \relative c' {
  g2^\f g 
  | % 89
  c g 
  | % 90
  g g 
  | % 91
  g1^\fermata 
  | % 92
  g2 g 
  | % 93
  as as 
  | % 94
  g2. ( f4 )
  | % 95
  es1^\fermata 
  | % 96
  g2 g 
  | % 97
  es g 
  | % 98
  g4 ( b!2 ) a4 
  | % 99
  f1^\fermata 
  | % 100
  f2 g 
  | % 101
  as as,4 ( b )
  | % 102
  c1 
  | % 103
  h2^\fermata r2
  r1*3
  r2^\fermata
  g'2^\p
  e 
  c f, 
  c' des2. des4
  c1^\fermata |
  as2 des 
  c c 
  | % 114
  c2. c4 
  | % 115
  d!1^\fermata 
  \bar "||"
}
  \relative c' {
  \barNumberCheck 116
  c2^\f c4 c 
  | % 117
  c2. c,4 
  | % 118
  c1^\fermata 
  | % 119
  c'2 c4 c 
  | % 120
  c1~ c2 f,2 c1~ |
  c2 r2 |
  c'4. c8 
  c4 c f,2 
  f4 c g'1 |
  c,4\breathe c' c b8 c 
  | % 128
  as2 g 
  | % 129
  f e 
  | % 130
  c1~ | c1~ | c2 c2 |
  c1~ | c1~ | c2 c2 |
  \barNumberCheck 136
  c4^\ff ( g' 
  c b as g 
  as f ) |
  b ( as ) g ( f ) |
  e2. e4 |
  f ( g ) as ( b ) |
  e,2 r4   e |
  f2. f4 |
  ges2. ( f4 |
  es2 ) es |
  f2. f4 
  | % 146
  b,1 
  | % 147
  r1 
  |
  \barNumberCheck 148
  f''2^\ff f 
  | % 149
  des2. des4 
  | % 150
  c ( g ) as ( b )
  | % 151
  a2 r2 
  | % 152
  d,! c 
  | % 153
  h1 
  | % 154
  b!
  | % 155
  a (
  | % 156
  as! 
  | % 157
  g1~ | g1~ | g2 c2 h )c
  g'1~ g~ g^\fermata
  \bar "||"
}
}

mitten_wir_three_basso_one = {
  \set Score.currentBarNumber = 164
  \relative c' {
    g2^\f g 
    | % 165
    c g 
    | % 166
    c, ( g 
    | % 167
    c ) c 
    | % 168
    g'1^\fermata
    | % 169
    g2 c, 
    | % 170
    f as, 
    | % 171
    g1 
    | % 172
    c^\fermata 
    | % 173
    g'2^\mf g 
    | % 174
    c g 
    | % 175
    c, c 
    | % 176
    d1^\fermata 
    | % 177
    g2 \> c, 
    | % 178
    as f' 
    | % 179
    g,4 (as b2 )
    | % 180
    \after 4 \! es2^\fermata r2 |
  }
  \relative c' {
    \barNumberCheck 181
    r2
    ^\markup {
      \epsfile #X #5 #"Mendelssohn - Mitten wir im Leben sind/tacet.eps"
    }
    g2^\f |
    g2. g4 |
    c,1 |
    g2^\fermata g'4 (f )
    | % 185
    e2 c 
    | % 186
    f c 
    | % 187
    des2. des4 
    | % 188
    c1^\fermata 
    | % 189
    as2 b 
    | % 190
    c c4 ( b )
    | % 191
    as2 as 
    | % 192
    g1^\fermata
    | % 193
    g'2^\p c,4 c 
    | % 194
    c1~ |
    c2 c4 ( d ) |
    e1 |
    f1~ \< |
    f2 \> f2 \! |
    c1^\fermata
    c1^\pp
    | % 201
    f2 c4 c |
    | % 202
    g'1 
    | % 203
    c,2 r2 |
    r2 c2^\p f, c' f^\cresc\breathe  es! |
    as  g4.^\f g8 |
    c2 g |
    c, c^\dim |
    c1 |
    f 
    | % 212
    g,^\p 
    | % 213
    g 
    | % 214
    c^\fermata
  }
  \relative c' {
    \barNumberCheck 215
    c2.^\p b!4 
    | % 216
    as2 g 
    | % 217
    f1 
    | % 218
    g2 r2 
    | % 219
    c2. b4 
    | % 220
    as2 g 
    | % 221
    f1 
    | % 222
    g1~ |
    g2 r2 |
    R1*2
    \barNumberCheck 226
    c,1\^\pp^>
    | % 227
    c^> 
    | % 228
    f^>
    | % 229
    g\espressivo
    \fermata
  }
}