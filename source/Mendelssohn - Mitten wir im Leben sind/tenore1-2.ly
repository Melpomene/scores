      \new Staff \with { instrumentName = "Tenor I" }
      << 
        \new Voice = "tenore1" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_two_tenore_one
        }
        \new Lyrics \lyricsto "tenore1" {
          Mit -- ten in dem Tod an -- ficht uns der Höl -- len __ Ra -- chen.
          Wer will uns aus sol -- cher Not frei und le -- dig ma -- chen?
          Es jam -- mert dein Barm -- her -- zig -- keit
          uns -- re Sünd und gro -- ßes Leid.
          \mitten_wir_text_h
          laß uns nicht ver -- za -- gen __ vor der tie -- fen Höl -- len Glut,
          vor der Höl -- len Glut.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son.
        }
      >>
