\new Staff \with { instrumentName = "Alt II" }
      <<
        \new Voice = "alto2" { 
          \mitten_wir_meta
          \mitten_wir_one_alto_two
        }
        \new Lyrics \lyricsto "alto2" {
          \mitten_wir_one_text_f
          \mitten_wir_text_h
          hei -- li -- ger Her -- re Gott, hei -- li -- ger star -- ker Gott, __
          hei -- li -- ger, hei -- li -- ger  barm -- her -- zi -- ger Hei -- land, du -- e -- wi -- ger Gott,
          in des bit -- tern To -- des Not, in des To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, Ky -- ri -- e e -- le -- i -- son, __ e -- le -- i -- son.
          Ky -- ri -- e e -- le -- i -- son, __ Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son. __
        }
      >>
