mitten_wir_one_alto_two = {
  \repeat unfold 3 { R1*3 r1\fermata }
  R1*3
  \relative c'' {
    \barNumberCheck 16
    r2^\fermata g2^\p |
    g2. g4 |
    g2 g |
    c,1 |
    g'2 \fermata r2 |
    \repeat unfold 2 { R1*3 r1^\fermata }
    \bar "||"
    
    \barNumberCheck 29
    g2^\f g4 g |
    g2. g4 |
    c1^\fermata |
    c2 c4 c |
    as1~ |
    as2 c4( h ) |
    c1~ |
    c2 r2 |
    c4. c8 c4 c |
    as2 as4 g |
    f1 |
    e1 |
  }

  \relative c'' {
    \barNumberCheck 41
    c2^\f c4 c |
    c4. c8 c2 |
    d2 d4 d |
    es!4. es8 es2 |
    d4. d8 d2
    
    \barNumberCheck 46
    c4. c8 c4 c |
    c2 c4 c |
    c1 |
    c2 c |
    c1 |
    c2 g | g1 |
  }
  \relative c {
    \barNumberCheck 53
    R1*2 |
    des'4^\f des des c |
    f2 e |
    as2 r2 |
    f2. e4 | 
    as2 as | 
    g2 r2 |
    
    \barNumberCheck 61
    f2^\ff f |
    des2. des'4 |
    c ( g ) as ( b ) |
    as2 r2 |
    c2 c |
    as2. as4 |
    g4 ( d! ) e ( f ) |
    e4 ( g as b |
    as g ) as2 |
    b4 ( as g f |
    c'4 b as ) g |
    f2 r2 |
  }
  \relative c'' {
    \barNumberCheck 73
    b2 b |
    g2. as4 |
    b2 b |
    g2. ( as4 |
    f2 ) r2 |
    e2 e |
    c2. c4 |
    c4 ( d! es! d |
    c2 ) c' |
    h2 \breathe c |
    g1~ ( |
    g2. fis4 |
    g2 ) c, |
    d2 ( g~ ) |
    g1\fermata \bar "||"
  }
}

mitten_wir_two_alto_two = {
  \set Score.currentBarNumber = 88
  \repeat unfold 3 { R1*3 r1^\fermata }
  R1*3
  \relative c'' {
    r2^\fermata
    g2^\p |
    g2. g4 |
    g2 g |
    c,1 
    | % 107
    g'2^\fermata r2 |
    \repeat unfold 2 { R1*3 r1^\fermata }
    \bar "||"
  }
  \relative c'' {
    \barNumberCheck 116
    g2^\f g4 g 
    | % 117
    g2. g4 
    | % 118
    c1^\fermata  
    | % 119
    c2 c4 c 
    | % 120
    as1~ |
    as2 c4 ( h ) |
    c1~ |
    c2 r2
    \barNumberCheck 124
    c4. c8 c4 c |
    as2 as4 g |
    f1 |
    e2 e 
    | % 128
    f ( g )
    | % 129
    as b 
    | % 130
    as r2 
    |
    \barNumberCheck 131
    des,4^\f des des c 
    | % 132
    f2 e 
    | % 133
    as r2 
    | % 134
    f2. e4 
    | % 135
    as2 as 
    | % 136
    g r2 
    |
    \barNumberCheck 137
    f2^\ff f 
    | % 138
    des2. des'4 
    | % 139
    c ( g ) as ( b )
    | % 140
    as2 r4 des 
    | % 141
    c ( g ) as ( b )
    | % 142
    as2 r2 
    |
    \barNumberCheck 143
    b2^\ff b 
    | % 144
    ges2. ges4 
    | % 145
    f ( c ) des ( es )
    | % 146
    des ( as' g ) f 
    | % 147
    e2. e4 
    | % 148
    f ( g ) as2 
    | % 149
    b4 ( as g f 
    | % 150
    c' b ) as ( g )
    | % 151
    c2 r4 c 
    | % 152
    h ( f ) g ( as )
    | % 153
    g2 r2 
    | % 154
    e e 
    | % 155
    c2. c4 
    | % 156
    c ( d! es! d 
    | % 157
    c2 ) c' 
    | % 158
    h c 
    | % 159
    g1~ |
    g2. ( fis4 g2 ) c, d ( g2~ )
    g1^\fermata
    \bar "||"
  }
}

mitten_wir_three_alto_two = {
  \set Score.currentBarNumber = 164
  \relative c' {
    r1 
    | % 165
    es2^\f d 
    | % 166
    c g' 
    | % 167
    g g 
    | % 168
    g1 \fermata
    | % 169
    g4 ( f ) es2 
    | % 170
    as f 
    | % 171
    g ( h, ) 
    | % 172
    c1 \fermata
    \barNumberCheck 173
    g'2^\mf g 
    | % 174
    es g 
    | % 175
    g g 
    | % 176
    d1 \fermata
    | % 177
    d2 \> c 
    | % 178
    f as 
    | % 179
    g ( b, )
    | % 180
    \after 4 \! b \fermata
  }
  \relative c'' {
    g^\f
    | % 181
    g2. g4 
    | % 182
    g2 g 
    | % 183
    g2. ( a4 )
    | % 184
    h2 \fermata d, 
    | % 185
    c e 
    | % 186
    f e 
    | % 187
    f b, 
    | % 188
    e1 \fermata
    | % 189
    f2 g 
    | % 190
    f e 
    | % 191
    f c 
    | % 192
    h1 \fermata
  }
  \relative c' {
    \barNumberCheck 193
    c2^\p c4 c 
    | % 194
    c2. c4 
    | % 195
    c1 
    | % 196
    c2 c4 c 
    | % 197
    f1~ \< |
    f2 \> c4 \! ( h ) |
    c1 \fermata
    c4.^\pp c8 c4 c 
    | % 201
    c2 c4 c 
    | % 202
    c2 ( h )
    | % 203
    c \breathe c^\p 
    | % 204
    c1~
    c2 e2 |
    f4 ( as2^\cresc ) \breathe g4 |
    as c2^\f g4 |
    g2 g4 ( f ) |
    es2 c^\dim |
    c1 
    | % 211
    c 
    | % 212
    c^\p
    | % 213
    d2 ( f )
    | % 214
    e1 \fermata |
    R1*2
  }
  \relative c' {
    c2. d4 
    | % 218
    e1~ |
    e2 r2 |
    r1 |
    c2. d4 
    | % 222
    e2 r2 
    | % 223
    c2.^\pp c4 |
    | % 224
    c2 c 
    | % 225
    \repeat unfold 3 { c1~ } c1
    e1^\espressivo
    \fermata
  }
}