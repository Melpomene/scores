mitten_wir_one_soprano_two = {
  \repeat unfold 3 { R1*3 r1\fermata }
  R1*3
  \relative c'' {
    \barNumberCheck 16
    r2^\fermata h2^\p |
    h2. h4 |
    h2 h |
    c4 ( d es2 ) | d2^\fermata r2
    \repeat unfold 2 { R1*3 r1^\fermata }
    
    \barNumberCheck 29
    e2^\f e4 e |
    e2. d4 |
    e1^\fermata |
    c2 c4 c |
    f1~ |
    f2 f |
    e1~ |
    e2 r2 |
    c4. c8 c4 c |
    c2 c4 c |
    d1 |
    e1 |
    
    \barNumberCheck 41
    c2^\f c4 c |
    c4. c8 c2 |
    d2 d4 d |
    es!4. es8 es2 |
    d4. d8 d2
    
    \barNumberCheck 46
    c4. c8 c4 c |
    c2 c4 c |
    c1 |
    c2 c |
    c1 |
    c2 h |
    c1 |
    
    \barNumberCheck 53
    R1*4
    r4 c4^\f c b8 c |
    as2 as4 g |
    c2 c | e, r2 |
  }
  
  \relative c'' {
    \barNumberCheck 61
    f2^\ff f |
    des2. des4 |
    c4 ( g ) as ( b ) |
    as2 r4 des4 |
    c4 ( g as b |
    c es d! c |
    g'1~ ) |
    g4 e c r |
    r2 f2~ ( |
    f4 des b g |
    e'2. ) e4
    f2 r |
    r1 |

    \barNumberCheck 74
    f4^\f ( e ) f2 |
    g,2 r4 f |
    f'4 ( e f2~ |
    f4 as g f |
    e4 f g2 |
    c,1~ |
    c4 h c d! |
    es!2. ) es4 |
    d2 c \breathe |
    g'1~ |
    g2 ( f4 es ) |
    es1 ( |
    d2 ) c |
    d1\fermata \bar "||"
  }
}

mitten_wir_two_soprano_two = {
  \set Score.currentBarNumber = 88
  \relative c ''{
    \repeat unfold 3 { R1*3 r1^\fermata }
    R1*3
    \barNumberCheck 103
    r2^\fermata h2^\p |
    h2. h4 |
    h2 h |
    c4 ( d es2 )
    | % 107
    d2^\fermata r2 
    \repeat unfold 2 { R1*3 r1^\fermata } |
    \barNumberCheck 116
    e2^\f e4 e 
    | % 117
    e2. d4 
    | % 118
    e1^\fermata
    | % 119
    c2 c4 c 
    | % 120
    c1 
    | % 121
    d 
    | % 122
    e1~ e2 r2 |
    c4. c8 c4 c |
    c2 c4 c |
    d1 |
    e2 g, 
    | % 128
    as ( b )
    | % 129
    c c4 ( des )
    | % 130
    c2 r2 |
    R1*2
    \barNumberCheck 133
    r4 c4^\f c b8 c |
    as2. ( g4 |
    f2 c' ) |
    c r2 
    |
  }
  \relative c'' {
    \barNumberCheck 137
    f2^\ff f 
    | % 138
    des2. des4 
    | % 139
    c ( g ) as ( b )
    | % 140
    as2 r4 des 
    | % 141
    c ( g as b 
    | % 142
    as es' des c 
    | % 143
    b f' es des 
    | % 144
    c des es c 
    | % 145
    a2. ) a4 
    | % 146
    b2 r2 |
    r1 |
    \barNumberCheck 148
    r2 f'2~^\ff ( |
    f4  des4 ) b ( g ) |
    c2  r4 c |
    c4 ( f, g a |
    h2 ) c |
    g4 r4  g2~ | 
    g2 e'2 |
    f2 ( c2~ |
    c4 h4 ) c ( d ) |
    es!2. es4 |
    d2\breathe c
    g'1~  |
    g2 ( f4 es ) |
    es1 ( |
    d2 ) c 
    | % 163
    d1^\fermata \bar "||"
  }
}

mitten_wir_three_soprano_two = {
  \set Score.currentBarNumber = 164
  \relative c'' {
  r1 
  | % 165
  g2^\f g 
  | % 166
  c c4 ( h )
  | % 167
  c2 c 
  | % 168
  h1 \fermata 
  | % 169
  d2 c 
  | % 170
  c c 
  | % 171
  c ( h )
  | % 172
  g1 \fermata 
  | % 173
  g2^\mf g 
  | % 174
  c h 
  | % 175
  es c 
  | % 176
  a1 \fermata
  | % 177
  g2 \> g 
  | % 178
  f f 
  | % 179
  b ( f )
  | % 180
  \after 4 \! es \fermata h'^\f
  | % 181
  h2. h4 
  | % 182
  h2 h 
  | % 183
  c4 ( d es2 )
  | % 184
  d \fermata g, 
  | % 185
  c b! 
  | % 186
  as g 
  | % 187
  f2. f4 
  | % 188
  g1 \fermata
  | % 189
  c2 b 
  | % 190
  as g 
  | % 191
  f2. f4 
  | % 192
  g1 \fermata
}
  \relative c' {
    \barNumberCheck 193
    e2^\p e4 e 
    | % 194
    e2. f4 
    | % 195
    g1 
    | % 196
    c,2 c4 c 
    | % 197
    as'1~ \< as2 \> f2 \! |
    e1 \fermata |
    c4.^\pp c8 c4 c 
    | % 201
    c2 c4 c 
    | % 202
    d1 
    | % 203
    e2 c^\p 
    | % 204
    d ( e )
    | % 205
    f g 
    | % 206
    as^\cresc \breathe b 
    | % 207
    c d4.^\f d8 
    | % 208
    es2 d 
    | % 209
    es c^\dim 
    | % 210
    c1~ |
    c4 ( h4 ) c2 |
    c1~^\p |
    c2 h2 |
    c1 \fermata |
    R1*2 |
  }
  \relative c'' {
    as2. as4 
    | % 218
    g1~ |
    g2 r2 |
    r1 |
    as2. as4 
    | % 222
    g2 r2 
    | % 223
    e2.^\pp e4 
    | % 224
    e2 e 
    | % 225
    f4 ( g as2~ )
    as1~ as1~ as1 |
    g1^\espressivo 
    \fermata
  }
}