\new Staff \with { instrumentName = "Sopran II" }
      <<
        \new Voice = "soprano2" { 
          \mitten_wir_meta
          \mitten_wir_three_soprano_two
        }
        \new Lyrics \lyricsto "soprano2" {
          \mitten_wir_three_text_s
	  Hei -- li -- ger Her -- re Gott,  hei -- li -- ger star -- ker Gott,
	  Hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
          du e -- wi -- ger Gott,
          laß uns nicht ent -- fal -- len von des rech -- ten Glau -- bens Trost.
          Ky -- ri -- e, __ Ky -- ri -- e,
          Ky -- ri -- e e -- lei -- son.
        }
      >>
