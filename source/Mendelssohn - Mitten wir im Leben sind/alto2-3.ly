\new Staff \with { instrumentName = "Alt II" }
      <<
        \new Voice = "alto2" { 
          \mitten_wir_meta
          \mitten_wir_three_alto_two
        }
        \new Lyrics \lyricsto "alto2" {
          \mitten_wir_three_text_s % sic!
	  Hei -- li -- ger Her -- re Gott,  hei -- li -- ger star -- ker Gott,
	  Hei -- li -- ger barm -- her -- zi -- ger Hei -- land,
          du ew' -- ger Gott,
          laß uns nicht ent -- fal -- len von des rech -- ten Glau -- bens __ Trost.
          Ky -- ri -- e, __ Ky -- ri -- e,
          Ky -- ri -- e e -- lei -- son.
        }
      >>
