\new Staff \with { instrumentName = "Sopran I" }
      <<
      \textMark \markup { \bold "Choral" }
        \new Voice = "soprano1" { 
          \mitten_wir_meta
          \mitten_wir_one_soprano_one
        }
        \new Lyrics \lyricsto "soprano1" {
          Das bist du, Herr, al -- lei -- ne. __
          \mitten_wir_text_h __
          du e -- wi -- ger Gott, du e -- wi -- ger Gott, __ du __ e -- wi -- ger Gott,
          laß uns nicht ver -- sin -- ken in des bit -- tern To -- des Not.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e, Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, e -- le -- i -- son.
        }
      >>
