      \new Staff \with { instrumentName = "Bass I" }
      <<
        \new Voice = "basso1" { 
          \mitten_wir_meta
          \clef "bass"
          \mitten_wir_two_basso_one
        }
        \new Lyrics \lyricsto "basso1" {
          \mitten_wir_two_text_m
          \mitten_wir_text_h
          laß uns nicht ver -- za -- gen vor der tie -- fen Höl -- len Glut. __
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, __ e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son. __
        }
      >>
