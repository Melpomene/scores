mitten_wir_one_tenore_one = {
  \relative c' {
          g2^\f g |
          c2 d |
          es2 es |
          d1\fermata |
          d2 es |
          f2 c4 ( d ) |
          es2 ( d ) |
          c1 \fermata |

          \barNumberCheck 9
          g2 g |
          c2 d |
          es2 es |
          d1\fermata |
          d2 es |
          f2 c4 ( b ) |
          as1 |
          g2\fermata r2 |
          R1*3

          \barNumberCheck 20
          r2\fermata g^\p |
          c2 b |
          as2 g |
          f2. f4 |
          g1\fermata |
          c2 b |
          as2 g |
          f2.^> f4 g1\fermata \bar "||"
        }
  \relative c' {
          \barNumberCheck 29
          e2^\f e4 e |
          e2. e4 |
          e1\fermata |
          c2 c4 c |
          f1~ |
          f2 c4 ( d ) |
          e1~ |
          e2 r2 |
          c4. c8 c4 e |
          f2 f4 e |
          d1 |
          c1~ |
          c2 r2 |

          \barNumberCheck 42
          f1^\f |
          f1 |
          b,1 |
          b2. b4
          c1 |
          f2 e |
          f2 ( c ) |
          r2 e |
          c1 |
          g'1 |
          g1 |
        }
  \relative c' {
          \barNumberCheck 53
          r4 c^\f c b8 c |
          as2 g |
          des'4 des des c |
          f2 e |
          f2 ( c ) |
          f,2. c'4 |
          c2 c | 
          c2 r2 |

          \barNumberCheck 61
          f2^\ff f |
          des2. des4 |
          c4 ( g ) as ( b ) |
          as2 r4 des4 |
          c4 ( g ) as ( g ) |
          f2 r2 |
        }
  \relative c'' {
          \barNumberCheck 67
          g2^\ff g |
          c, r2 |
          f2 f |
          des2. des4 |
          c4 ( g ) as ( b ) |
          as2 f' |
          f1~ |
          f1~ |
          f2 f, |
          f2 r2 |

          \barNumberCheck 77
          f2 f |
          b2. b4 |
          c1 (
          as!1 |
          c2 ) es |
          f2 \breathe es |
          d2 ( c |
          g es' |
          c2 g'~ )
          g2 es |
          d1\fermata \bar "||"
        }
}

mitten_wir_two_tenore_one = {
  \set Score.currentBarNumber = 88
  \relative c' {
  g2^\f g 
  | % 89
  c d 
  | % 90
  es es 
  | % 91
  d1^\fermata 
  | % 92
  d2 es 
  | % 93
  f c4 ( d )
  | % 94
  es2 ( d )
  | % 95
  c1^\fermata 
  | % 96
  g2 g 
  | % 97
  c d 
  | % 98
  es es 
  | % 99
  d1^\fermata 
  | % 100
  d2 es 
  | % 101
  f c 
  | % 102
  b ( as )
  | % 103
  g^\fermata r2 |
  R1*3
  \barNumberCheck 107
  r2^\fermata g2^\p c 
  b as 
  g f2. f4
  g1^\fermata
  c2 b 
  as g 
  f2. f4 
  g1^\fermata
  | % 116
  e'2^\f e4 e 
  | % 117
  e2. e4 
  | % 118
  e1^\fermata 
  | % 119
  c2 c4 c 
  | % 120
  f1~ |
  f2 f2
  e1~ |
  e2 r2
  \barNumberCheck 124
  c4. c8 
  c4 e f2 
  f4 e d1 c2 r2 |
  \barNumberCheck 128
  r4 c4 c b8 c as2 ( g ) f ( e ) |
  des'4^\f des des c f2 e f ( c ) |
  f,2. c'4 |
  c2 c c r2 |
  \barNumberCheck 137
  f2^\ff f 
  | % 138
  des2. des4 
  | % 139
  c ( g ) as ( b )
  | % 140
  as2 r4 des 
  | % 141
  c ( g as b 
  | % 142
  as c f2 
  | % 143
  des ) b 
  | % 144
  ges' r2 
  | % 145
  f f 
  | % 146
  des2. des4 
  | % 147
  c ( g ) as ( b )
  | % 148
  as2 \breathe as 
  | % 149
  f' ( des 
  | % 150
  b4 g ) e2 
  | % 151
  f r2 
  | % 152
  f' f 
  | % 153
  d!2. d4 
  | % 154
  c ( g as b 
  | % 155
  c1 
  | % 156
  as! 
  | % 157
  c2 ) es 
  | % 158
  f \breathe es 
  | % 159
  d ( c 
  | % 160
  g es' 
  | % 161
  c g'2~ |
  g2 ) es2
  d1\fermata \bar "||"
}
}

mitten_wir_three_tenore_one = {
  \set Score.currentBarNumber = 164
  \relative c' {
    g2^\f g 
    | % 165
    c d 
    | % 166
    es ( d 
  | % 167
    c ) g4 ( a )
    | % 168
    h1^\fermata 
    | % 169
    h2 c 
    | % 170
    c f, 
    | % 171
    c' ( d )
    | % 172
    es1^\fermata
    \barNumberCheck 173
    g,2^\mf g 
    | % 174
    es' d 
    | % 175
    c es 
    | % 176
    a,1^\fermata
    | % 177
    h2 \> c 
    | % 178
    c c 
    | % 179
    g ( f ) 
    | % 180
    \after 4 \! g2^\fermata r2
  }
  \relative c' {
    \barNumberCheck 181
    r2
    ^\markup {
      \epsfile #X #5 #"Mendelssohn - Mitten wir im Leben sind/tacet.eps"
    }
    d2^\f |
    d2. d4 c1 |
    d2^\fermata h  
    | % 185
    g c 
    | % 186
    c c4 ( b )
    | % 187
    as2 f 
    | % 188
    c'1^\fermata 
    | % 189
    c2 des 
    | % 190
    c c 
    | % 191
    c c, 
    | % 192
    d1^\fermata
  }
  \relative c {
    \barNumberCheck 193
    e2^\p g4 g 
    | % 194
    g2. ( f4 
    | % 195
    e2 ) g 
    | % 196
    c,1 
    | % 197
    c'1~ \< |
    c2 \> as2 \! |
    c1\fermata |
    c,2.^\pp ( g'4 )
    | % 201
    as2 g4 g 
    | % 202
    g1 
    | % 203
    g2 g^\p 
    | % 204
    f ( g )
    | % 205
    as! c 
    | % 206
    c^\cresc \breathe es! 
    | % 207
    es g4.^\f g8 
    | % 208
    es2 h 
    | % 209
    c c4^\dim ( b! )
    | % 210
    a1 
    | % 211
    as!2 ( c )
    | % 212
    c^\p ( es )
    | % 213
    g,1 |
    g ^\fermata
  }
  \relative c' {
    \barNumberCheck 215
    c2.^\p b!4 
    | % 216
    as2 g 
    | % 217
    as4 ( b ) c2~ |
    c2 r2 |
    c2. b4 as2 g as4 ( b ) c2~
    c1~ c2 r2 |
    R1*2
    as1^\pp^> 
    | % 227
    as^>
    | % 228
    as^>
    | % 229
    c^\espressivo
    \fermata
  }
}