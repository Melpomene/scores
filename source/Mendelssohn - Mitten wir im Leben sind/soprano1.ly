mitten_wir_one_soprano_one = {
  \repeat unfold 3 { R1*3 r1\fermata }
  R1*3
  \relative c'' {
    \barNumberCheck 16
    r2\fermata d2^\p |
    d2. d4 |
    d2 d |
    es4 ( f ) g2~ |
    g2 \fermata r2 |
    \repeat unfold 2 { R1*3 r1\fermata } \bar "||"
    
    \barNumberCheck 29
    e2^\f^\markup { \bold { Vivace } } e4 e4 |
    e2. f4 |
    g1\fermata
    c,2 c4 c |
    as'1~ |
    as2 as |
    g1~ |
    g2 r2 |
    
    \barNumberCheck 37
    c,4. c8 c4 c |
    c2 c4 c |
    d1 |
    e1~ |
    e2 r2 |
    r2 c2^\f |
    c2 b |
    b1 |
    b2. b4 |
    
    \barNumberCheck 46
    c2 ( b ) |
    as g |
    c2 ( b ) |
    as2 ( g ) |
    c1 |
    d2 d |
    e1 |
    R1*3
  }
  \relative c'' {
    \barNumberCheck 56
    r4 c^\f c b8 c |
    as2 g |
    des'4 des des c |
    f2. f4 |
    e2 r2 |

    \barNumberCheck 61
    f2^\ff f |
    des2. des4 |
    c4 ( g ) as ( b ) |
    as2 r4 des |
    c4 ( g as b |
    c es d! c |
    g'1~ ) |
    g4 e c r4 |
    r2 f2~ ( |
    f4 des b ) g |
    c2 r2 |

    \barNumberCheck 72
    r1 |
    f4^\f ( e ) f2 |
    g,2 r4 f4 |
    f'4 ( e ) f2 |
    g,2 r4 f |
    f'4 ( as g f |
    e4 f g2 |
    c,1~ |
    c4 h c d |
    es!2. ) es4 |
    d2 \breathe c |
    g'1~ ( |
    g2  f4 es  |
    es1  |
    d2 ) c |
    d1\fermata
  }
}

mitten_wir_two_soprano_one = {
  \set Score.currentBarNumber = 88
  \repeat unfold 3 { R1*3 r1^\fermata }
  R1*3
  \relative c'' {
    \barNumberCheck 103
    r2^\fermata d2^\p d2. d4 d2 d
    es4 ( f )  g2~ |
    g2^\fermata r2 |
    \repeat unfold 2 { R1*3 r1^\fermata } \bar "||"
    \barNumberCheck 116
    e2^\f^\markup { \bold Vivace } e4 e 
    | % 117
    e2. f4 
    | % 118
    g1^\fermata 
    | % 119
    c,2 c4 c 
    | % 120
    c1 
    | % 121
    f 
    | % 122
    g1~ g2 r2 c,4. c8 
    c4 c c2 
    c4 c d1 e2 g, 
    as ( b )
    c c4 ( des )
    c2 r2 |
    r1 |
    \barNumberCheck 132
    r4 c4^\f c b8 c as2 g des'4 des 
    des c f2 
    f e 
    r2
  }
  \relative c'' {
    f2^\ff  f des2. des4 c ( g ) as ( b ) as2 r4 
    des c ( g as  b as es' des 
    c b f' es 
    des c des es 
    c a2. )
    a4 b2 r2
    R1*3
    \barNumberCheck 150
    g'2 g 
    | % 151
    c,2. c4 
    | % 152
    f2 f 
    | % 153
    g, r2 
    | % 154
    e' e 
    | % 155
    f (c2~ | c4  h4 ) c ( d! ) |
    es!2. es4 d2 c
    g'1~ g2 ( f4 es ) es1 ( d2 )  c |
    | % 163
    d1^\fermata \bar "||"
  }
}

mitten_wir_three_soprano_one = {
  \set Score.currentBarNumber = 164
  \relative c'' {
    | % 164
    r1 
    | % 165
    g2^\f g 
    | % 166
    c d 
    | % 167
    es es 
    | % 168
    d1^\fermata 
    | % 169
    d2 es 
    | % 170
    f c4 ( d )
    | % 171
    es2 ( d )
    | % 172
    c1^\fermata 
    | % 173
    g2^\mf g 
    | % 174
    c d 
    | % 175
    es es 
    | % 176
    d1^\fermata 
    | % 177
    d2 \> es 
    | % 178
    f c 
    | % 179
    b ( as )
    | % 180
    \after 4 \! g^\fermata d'^\f
    | % 181
    d2. d4 
    | % 182
    d2 d 
    | % 183
    es4 ( f ) g2~ |
    g2^\fermata g,2 c 
    b! as 
    g f2. f4 |
    g1^\fermata |
    c2 b 
    as g 
    f2. f4 
    g1 ^\fermata
    | % 193
    e2^\p e4 e 
    | % 194
    e2. f4 
    | % 195
    g1 
    | % 196
    c,2 c4 c 
    | % 197
    as'1~ \< | as2 \> as2 \! |
    g1^\fermata
    c,4.^\pp c8 c4 c 
    | % 201
    c2 c4 c 
    | % 202
    d1 
    | % 203
    e2 c^\p 
    | % 204
    d ( e ) 
    | % 205
    f g 
    | % 206
    as^\cresc \breathe b 
    | % 207
    c d4.^\f d8 
    | % 208
    es2 d 
    | % 209
    es e^\dim 
    | % 210
    f ( c2~ |
    c4 h4 ) c ( d ) |
    es1^\p d 
    | % 214
    c^\fermata 
    | % 215
    R1*2 |
    \barNumberCheck 217
    c2 c 
    | % 218
    c1~ |
    c2 r2 |
    r1 |
    c2 c 
    | % 222
    c r2 
    | % 223
    g2.^\pp g4 
    | % 224
    g2 g 
    | % 225
    as4 ( b c2 ) |
    c1~ c1~ c1 |
    c1^\espressivo
    \fermata
  }
}