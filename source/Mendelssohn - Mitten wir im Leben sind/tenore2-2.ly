      \new Staff \with { instrumentName = "Tenor II" }
      << 
        \new Voice = "tenore2" { 
          \mitten_wir_meta
          \clef "G_8"
          \mitten_wir_two_tenore_two
        }
        \new Lyrics \lyricsto "tenore2" {
          \mitten_wir_two_text_m
          \mitten_wir_text_h
          laß uns nicht ver -- za -- gen __ vor der tie -- fen Höl -- len Glut.
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, __ e -- le _ -- i -- son.
        }
      >>
