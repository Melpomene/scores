mitten_wir_one_tenore_two = {
  \relative c' {
  g2^\f g |
  c2 h |
  c2 c |
  h1^\fermata |
  h2 c |
  c2 c |
  c2 ( h ) |
  g1^\fermata |
  
  \barNumberCheck 9
  g2 g |
  g2 h |
  c2 c |
  b!1^\fermata |
  b2 b |
  c2 as4 ( g ) |
  f1 |
  d2^\fermata r2 |
  R1*3
  
  \barNumberCheck 20
  r2^\fermata g2^\p |
  g2 e |
  f2 e |
  f2. f4 |
  e1^\fermata |
  f2 g |
  f2 e |
  f2^> c' | %% TODO eckige Klammern um c2 h1
  h1^\fermata |

  \barNumberCheck 29
  g2^\f g4 g |
  g2. g4 |
  g1^\fermata |
  c2 c4 c |
  as1~ |
  as2 f' |
  g,1~ |
  g2 r2 |
  c4. c8 c4 c |
  c2 c4 g' |
  g1 |
  g,1 |

  \barNumberCheck 41
  c2^\f c4 c |
  c4. c8 c2 |
  d2 d4 d |
  es!4. es8 es2 |
  d4. d8 d2 |
  c4. c8 c4 c |
  c2 c4 c |
  c1 |
  c2 c |
  f,1 |
  d'1 |
  c1 |
  r1

  \barNumberCheck 54
  r4 c^\f c b8 c |
  as2. g4 |
  c2 b |
  c2. g4 |
  as2. ( e4 |
  f2 ) as |
  c2 r2 |
}
  \relative c' {
          \barNumberCheck 61
          f2^\ff f |
          des2. des4 |
          c4 ( g ) as ( b ) |
          as2 r4 des4 |
          c4 ( g ) as ( g ) |
          f2 r2 |
          r1
        }
  \relative c' {
          \barNumberCheck 68
          e2^\ff e |
          c2. as4 |
          f'2 ( des |
          b2 c4 ) des |
          c4 ( b ) \breathe as2 |
          des2. c4 |
          f2 r4 as, |

          \barNumberCheck 75
          des2 des4 ( c ) |
          f2. f4 |
          d!1 ( |
          des1 |
          c2 a |
          f'2 c~ |
          c2 g~ |
          g1 |
          f'2 es |
          d2 g,~ |
          g2. a4 |
          h2 ) c |
          h1\fermata \bar "||"
        }
}

mitten_wir_two_tenore_two = {
  \set Score.currentBarNumber = 88
  \relative c' {
    g2^\f g 
    | % 89
    c h 
    | % 90
    c c 
    | % 91
    h1 \fermata
    | % 92
    h2 c 
    | % 93
    c c 
    | % 94
    c ( h )
    | % 95
    g1 \fermata 
    | % 96
    g2 g 
    | % 97
    g h 
    | % 98
    c c 
    | % 99
    b!1 \fermata
    | % 100
    b2 b 
    | % 101
    c as 
    | % 102
    g ( f )
    | % 103
    d \fermata r2 |
    R1*3
    \barNumberCheck 107
    r2^\fermata g^\p
    g e f e f2. f4 e1^\fermata
    f2 g 
    f e 
    f c' 
    h1^\fermata
    \bar "||"
  }
  \relative c' {
    \barNumberCheck 116
    g2^\f g4 g 
    g2. g4 
    g1^\fermata 
    c2 c4 c 
    as1~ |
    as2 f'2
    g,1~ |
    g2 r2
    \barNumberCheck 124
    c4. c8 
    c4 c c2 
    c4 g' g1 g,2
    r2 r1 |
    \barNumberCheck 129
    r4 c4 c b8 c |
    as2 ( g ) |
    f ( e ) c' 
    b c2. g4 |
    as2. ( e4 f2 ) as c r2
  }
  \relative c' {
    \barNumberCheck 137
    f^\ff f 
    | % 138
    des2. des4 
    | % 139
    c ( g ) as ( b )
    | % 140
    as2 r4 des 
    | % 141
    c ( g as b 
    | % 142
    as c f2 
    | % 143
    des b 
    | % 144
    ges' es 
    | % 145
    c2. ) c4 
    | % 146
    b2 r2 
    | % 147
    c c 
    | % 148
    f2. f,4 
    | % 149
    f2 ( b 
    | % 150
    g1 
    | % 151
    a2 b4 c 
    | % 152
    d!2 f 
    | % 153
    g, ) g2~ |
    g4  des'4 ( c2 ) |
    r2 a
    f'2 ( c2~ |
    c2 g2~ |
    g1 |
    f'2 es | 
    | % 160
    d ) g,2~ (
    g2. a4 h2 ) c |
    h1^\fermata
  }
}


mitten_wir_three_tenore_two = {
  \set Score.currentBarNumber = 164
  \relative c' {
    | % 164
    g2^\f g 
    | % 165
    c h 
    | % 166
    es ( f 
    | % 167
    es4 d) c2 
    | % 168
    d1^\fermata
    | % 169
    g,2 g 
    | % 170
    as c 
    | % 171
    g2. ( f4 )
    | % 172
    es1^\fermata
    | % 173
    g2^\mf g 
    | % 174
    es' h 
    | % 175
    c4 ( g2 ) c4 
    | % 176
    fis,1^\fermata
    | % 177
    g2 \> g 
    | % 178
    as as 
    | % 179
    b1 
    | % 180
    \after 4 \! b2^\fermata r2
  }
  \relative c' {
    \barNumberCheck 181
    r2
    ^\markup {
      \epsfile #X #5 #"Mendelssohn - Mitten wir im Leben sind/tacet.eps"
    }
    h2^\f |
    h2. h4 es1 |
    g,2^\fermata h 
    | % 185
    g c 
    | % 186
    c c4 ( b )
    | % 187
    as2 f 
    | % 188
    c'1^\fermata 
    | % 189
    c2 des 
    | % 190
    c c 
    | % 191
    c c, 
    | % 192
    d1^\fermata
  }
  \relative c {
    \barNumberCheck 193
    c2^\p e4 e 
    | % 194
    e2. ( d4 
    | % 195
    c2)  e4 ( f )
    | % 196
    g1 
    | % 197
    as1~ \< |
    as2\> f2 \! |
    g1^\fermata
    c2.^\pp ( e,4 )
    | % 201
    f2 e4 e 
    | % 202
    g1 
    | % 203
    g2 e^\p 
    | % 204
    f ( g )
    | % 205
    as! g 
    | % 206
    f^\cresc \breathe es'! 
    | % 207
    es g4.^\f g8 
    | % 208
    es2 h 
    | % 209
    c c,^\dim 
    | % 210
    f1 
    | % 211
    as! 
    | % 212
    g^\p 
    | % 213
    g2 ( d )
    | % 214
    e1^\fermata
  }
  \relative c' {
    \barNumberCheck 215
    c2.^\p b!4 
    | % 216
    as2 g 
    | % 217
    as1 
    | % 218
    c2 r2 
    | % 219
    c2. b4 
    | % 220
    as2 g 
    | % 221
    as1 
    | % 222
    c1~ |
    c2 r2 |
    R1*2
    f,1^\pp^>
    | % 227
    f^>
    | % 228
    c^>
    | % 229
    e^\espressivo
    \fermata
  }  
}
