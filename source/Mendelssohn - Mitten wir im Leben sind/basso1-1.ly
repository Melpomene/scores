\new Staff \with { instrumentName = "Bass I" }
      <<
        \new Voice = "basso1" { 
          \mitten_wir_meta
          \clef "bass"
          \mitten_wir_one_basso_one
        }
        \new Lyrics \lyricsto "basso1" {
          \mitten_wir_one_text_m
          \mitten_wir_text_h
          hei -- li -- ger Her -- re Gott, hei -- li -- ger star -- ker Gott,
          hei -- li -- ger barm -- her -- zi -- ger Hei -- land, du -- ew' -- ger Gott,
          laß uns nicht ver -- sin -- ken in des bit -- tern To -- des Not. __ 
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son,
          Ky -- ri -- e e -- le -- i -- son, e -- le -- i -- son, e -- le -- i -- son
          e -- le -- i -- son. __
        }
      >>
