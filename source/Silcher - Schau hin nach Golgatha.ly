\version "2.24.3"
\language "deutsch"

#(set-default-paper-size "a4")

\paper {
  system-system-spacing = #'((basic-distance . 1) (padding . 5))
  ragged-last-bottom = ##f
  ragged-bottom = ##f
%%%  bottom-margin = 0\mm
%%%				%  indent = 0\cm
%%%   page-count = #3
}

\header {
  title = "Schau hin nach Golgatha"
  composer = "Friedrich Silcher (1789 – 1860)"
  poet = "unbekannt"
  opus = "op. 9 Nr. 4"
  tagline = "Konkordien-Kantorei Mannheim 2024 (rev. 1a) – Vervielfältigung und Verbreitung erlaubt"
}

global = {
  \autoBeamOff
  \dynamicUp
  \tempo "Sehr langsam"
  \key c \minor
  \time 4/4
  \partial 4
  \set Score.rehearsalMarkFormatter = #format-mark-box-alphabet
}

cantus = \relative c' {
  c4\mp | es \< g c4. c8 | h2.\mf\> \breathe
  g4 \! | h d\< f4. f8  | es2\> r4
  % \mark d\default
  es4\p | d c h c | g1~ | g2. g4 | as4. \< c8 f4 \! d | c2\> h4 \breathe
  g4\p g4.-> f8 es4 f es2\> d4 \breathe
  % \mark \default
  g4 \mf | es'4. c8 b!4 b | b2.~ \> b8[ as] \! | g2 r4
  g4\mf c4. c8 es4 es d2.~ \> d8 [ c] | b4 \breathe
  g4\p \< g   g \! | a2( c4) a | g2.( \> fis4 ) \! | g2 r4
  % \mark \default
  \barNumberCheck 20
  g\f | es'2. \fermata
  es,8\p[ f] | g2\fermata \> r4 \fermata
  c,4\mp | es g \< c4. c8\mf | h2. \> \breathe
  g4 | h \< d f4.\f \> f8 | es2 \! r4
  % \mark \default
  es4\p d4\( c h c \) | g1~ | g2 r4
  \barNumberCheck 29
  g \< as2 \> g \! f r4
  g4\p \< | f2 \> es \! d r4
  % \mark \default
  d'\p | d2->( \< f4 \!) es | es2\> d4\! \breathe
  h | h2\<( d4) \! c | c2\> h4 \breathe
  % \mark \default
  d\mf | es4. es8 c4 \> g\! | as2.\breathe
  f4\mf | g4. g8\> g4. f8 \! | es2. \p \breathe
  c'4 | as2. f4 | es2 \> d \! | c2. \fermata
  \bar "|."
}
altus = \relative c' {
  c4\mp | c \<  es es4. f8 | g2.\mf \> \breathe
  g4 \! g g \< g4. g8 | g2\> r4
  g4\p f es d c | h g'\( f es | d2. \)
  es4 | f4. \< as8 as4 \! as | g2\> g4\breathe
  d\p | d4.-> d8 c4 d | c2 \> h4
  g'\mf | es4. es8 f4 g | f1 | es2 r4
  g\mf | g4. g8 g4 g | g4 (\> fis8[ e]  fis4 d ) \! | d
  d\p \< d \! d | es2. es4 | d1 \> | d2 \! r4
  \barNumberCheck 20
  g\f | g2. \fermata c,4\p h2\fermata \> r4 \fermata
  c4\mp | c es \< es4. f8 \mf | g2. \> \breathe
  g4\< | g g g4. \f \> g8 | g2 \! r4
  g\p | f es d c | h( g' f es | d2 ) r4
  \barNumberCheck 29
  es \< f2 \> es \! | d2 r4
  es\p \< d2 \> c h2 \! r4
  g'\p | g2.\< g4\> | g2\> g4 \! \breathe
  g | g2. \< g4 \! | g2 \> g4 \breathe
  g\mf | g4. g8 g4 \> g \! |f2. \breathe
  f4\mf | es4. es8 \> d4 d\! | c2. \p \breathe
  g'4 | f2. d4 | c2\> h \! c2.\fermata
}
tenor = \relative c {
  es4\mp | g \< c c4. c8 | d2. \mf \> \breathe
  h4 \! | d h \< d4. d8 | c2 \> r2 \!
  r2 r4 es\p d c h c | h2.
  c4 | c4. \< c8 \! c4 d | es2 \> d4\breathe
  h\p | g4.-> g8 g4 as | g2 \> g4
  g\mf | g4. g8 b4 es | es ( d8[ c] d4 b) | b2 r4
  b\mf | c4. c8 c4 c | a1 \> | b4
  b\p \< d \! b | c2( a4) c | b2( \> a) b2 \! r4
  \barNumberCheck 20
  g\f | c2. \fermata g4\p g2 \fermata \> r4 \fermata
  es4\mp | g c4 \< c4. c8\mf | d2. \> h4 | d\< h d4. \f \> d8 | c2 \! r2
  r2 r4 es\p | d c h c h2 r  R1*3
  \barNumberCheck 33
  r2 r4 h\p | h2( \< d4) \! c | c2 \> h4 \! \breathe
  d | d2( \< f4 ) \! es | es2 \> d4 \breathe
  h\mf | c4. c8 c4\> c \! | c2. \breathe
  c4\mf | c4. c8 \> c4 h \! | c2.\p \breathe
  c4 | c2. as4 | g2\> f \! es2. \fermata
}
bassus = \relative c {
  c4\mf | c \< c as'4. as8 | g2. \mf \> \breathe
  g4 \! | g g \< h,4. h8 | c2 \> r2 \!
  r2 r4 g'\p | f es d c | g2.
  c4 | f4. \< f8 \! f4 f | g2\> g4\breathe
  g,\p | h4.-> h8 c4 f | g2 \> g,4
  g'\mf | c,4. c8 d4 es | b2.( d4) | es2 r4
  es\mf | es4. es8 c4 c | d2.( \> fis4 \! ) | g4
  g\p\< b \! g | c,!2. c4 | d1 \> | <g g,>2 \! r4
  \barNumberCheck 20
  g\f | c,2. \fermata c4\p g2 \fermata \> r4 \fermata
  c\mp | c c \< as'4. as8\mf | g2.\breathe
  g4 | g \< g \! h,4. \> h8 | c2 \! r2
  r2 r4 g'\p | f es d c | g2 r2 | R1*7
  \barNumberCheck 37
  r2 r4 g'\mf | c,4. c8 es4\> es \! | f2. \breathe
  as4\mf | g4. g8\> g4 g\! | as2.\p \breathe
  es4 | f2. f4 | g2\> g,\! | c2.\fermata
  
}
lyricscantus = \lyricmode {
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Dort schwebt am Kreu -- zes -- stamm,
  im To -- des -- kampf dein Je -- sus,
  im To -- des -- kampf dein Je -- sus,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den.
  Schau hin, schau hin!
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Er neigt sein ster -- bend Haupt.
  Es bricht sein Herz,
  es bricht sein Herz.
  Selbst En -- gel wei -- nen,
  selbst En -- gel wei -- nen
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod.
}
lyricsaltus = \lyricmode {
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Dort schwebt am Kreu -- zes -- stamm,
  am Kreu -- zes -- stamm,
  im To -- des -- kampf dein Je -- sus,
  im To -- des -- kampf dein Je -- sus,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den.
  Schau hin, schau hin!
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Er neigt sein ster -- bend Haupt.
  Es bricht sein Herz,
  es bricht sein Herz.
  Selbst En -- gel wei -- nen,
  selbst En -- gel wei -- nen
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod.
}
lyricstenor = \lyricmode {
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Dort schwebt am Kreu -- zes -- stamm,
  im To -- des -- kampf dein Je -- sus,
  im To -- des -- kampf dein Je -- sus,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den.
  Schau hin, schau hin!
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Er neigt sein ster -- bend Haupt.
  Selbst En -- gel wei -- nen,
  selbst En -- gel wei -- nen
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod.
}
lyricsbass = \lyricmode {
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Dort schwebt am Kreu -- zes -- stamm,
  im To -- des -- kampf dein Je -- sus,
  im To -- des -- kampf dein Je -- sus,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den,
  mit dei -- ner Schuld be -- la -- den.
  Schau hin, schau hin!
  Schau hin nach Gol -- ga -- tha,
  schau hin nach Gol -- ga -- tha!
  Er neigt sein ster -- bend Haupt.
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod,
  des Welt -- er lö -- sers Tod.
}


\score {
  \new ChoirStaff <<
    \new Staff <<
      \new Voice = "soprano" <<
        \global
        \cantus
      >>
      \new Lyrics {
	\lyricsto "soprano" 
	\lyricscantus
      }
    >>
    \new Staff <<
      \new Voice = "alto" <<
        \global
        \altus
      >>
      \new Lyrics {
	\lyricsto "alto" 
	\lyricsaltus
      }
    >>
    \new Staff <<
      \clef "G_8"
      \new Voice = "tenor" <<
        \global
        \tenor
      >>
      \new Lyrics {
	\lyricsto "tenor"
	\lyricstenor
      }
    >>
    \new Staff <<
      \clef bass
      \new Voice = "bass" <<
        \global
        \bassus
      >>
      \new Lyrics {
	\lyricsto "bass" 
	\lyricsbass
      }
    >>
  >>
}

