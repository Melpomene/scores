\version "2.20.0"
\language "deutsch"

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}
    
\header {
  title = "Übers Gebirg Maria geht"
  poet = "„Geistliche Lieder“, Erfurt 1575"
  composer = "Musik: Johann Eccard (1553–1611)"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
      \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key es \major
  \time 4/4
}


voiceSI = \relative c'' {
  \clef "treble"
  \autoBeamOff
  R1 b2 es 4 d | c b c4. c8 | b2 r2 |
  r4 b g a | b g f4. f8 es1 | R1 |
  b'2 es4 d | c b c4. c8 | b2 r2 |
  r4 b g a | b g f4. f8 es2 r2 |
  r4 f b b | as g g f | g 
  d' es4. es8  | d4 g2 f4~ | f4 es4.( d16[ c] d4) | es2
  %% Refrain
  r4 b | f'4. f8 f4 es | d2 d4 es | c4. c8 c4 b | a( b4. a16[ g] a4) | b2
  r4 c | c c f2 f4 es2 des4 | c2
  r4 c | b4. as8 g4 es'| d4. c8 b4 as | g2 f | es
  r4 b'| g4. f8 es4 c | f4. es8 d4 f4~ | f es2 d4 | es1 \bar "|."
}
voiceSII = \relative c'' {
  \clef "treble"
  \autoBeamOff
  g2 b4 as | g8([ f g as] b2) | a4 b2 a4 | b4 b4 f g | as f es4. es8 | f4 b b4. b8 | g1 |
  g2 b4 as | g8([ f g as ] b2) | a4 b2 a4 | b b f g | as f es4. es8 | f4 b b4. b8 | g2 r2 | R1
  r2 r4 c | h4. h8 c2 | g2. c4 | b1 | b2
  %% Refrain
  r4 es | d4. d8 d4 c | h2 h4 c | es2
  r4 es | c b c4.( f,8) | f2
  r4 f | g as f2 | f4 c'2 b4 | g1 | r2
  r4 b b4. as8 g4 f~| f es2 d4 | es8([ f g as] b2) |
  r4 b c2 | r2 f, | b2. b4 | b1
}

voiceA = \relative c' {
  \clef "treble"
  \autoBeamOff
  es2 es4 es | es4.( f8 g4) f | f2 f | f4.( es8 d4) es4~| es b2 c4 | d es es d | es1 |
  es2 es4 es | es4.( f8 g4) f | f2 f | f4.( es8 d4) es4~| es b2 c4 | d es es d | es 
  es g g | f2. e4 | f es! c c | d4. d8 c4 g'~( | g8 f d4) e4 f~( | f g8[ as! ] b2) | g1
  %% Refrain
  R1 | r2 r4 g | as4. as8 as4 g | f1 | d2
  r4 c | c c des2~ | des4 c4 f2~| f4 es c g~( | g8 as) b[ c] d4
  g | f4. es8 d2 |
  r2 r4 b'| g4. f8 es4 d | c( b2) a4 | b2. c4 d es f4. f8 | g1
}

voiceT = \relative c' {
  \clef "G_8"
  \autoBeamOff
  b2 b4 c | b1 | c4 d c2 | d r4 b | es, f g es | b'2 b | b1 |
  b2 b4 c | b1 | c4 d c2 | d r4 b4 | es, f g es | b'2 b | b4
  b4 es es | d4. c8 d4 b | c2 r2 |
  r2 r4 c | d4. d8 c2 | d4 es f2 | es2
  %% Refrain
  r4 es | b4. b8 b4 c | g'2 g |
  r4 c, es4. es8 | es4 d c2 | b
  r4 as! | g f as2 | as r2 |
  c4. d!8 es4 es | d4. c8 b4 g | b1 | b1 | es,2 r2 | r2
  r4 es'| d4. c8 b4 as | g2 f | es1
}

voiceB = \relative c {
  \clef "bass"
  \autoBeamOff
  es2 g4 as | es2 es4 b'| f1 | b2 r4
  es,4 | c d es c | b2. b4 | es1 |
  es2 g4 as | es2 es4 b' | f1 b2 r4
  es,4 c d es c b2. b4 | es2 r4
  es | b' b g2 | f4 c' as2 | g2 r4
  c | h4. h8 c4 a | b!2 b | es,1
  %% Refrain
  R1 | r2 r4 c'| as4. as8 as4 es | f1 | b,2
  r4 f'| e f des2 | des4 as'2 b4 | c c,4.( d!8) es[ f] | g1
  R1*2 | r4 b g4. f8 | es4 d c2 | b1~ | b | es
}


wordsISI = \lyricmode {
  \set stanza = "1. "
  Ü -- bers Ge -- birg Ma -- ri -- a geht
  zu ih -- rer Bas E -- li -- sa -- beth.
  Sie grüßt die Freun -- din, die vom Geist
  freu -- dig be -- wegt Ma -- ri -- a preist
  und sie des Her -- ren Mut -- ter nennt:
  Ma -- ri -- a ward fröh -- lich __ und __ sang:
  %% Refrain
  Mein Seel den Herrn er -- he -- bet, mein Geist sich Got -- tes freu -- et;
  er ist mein Hei -- land, fürch -- tet ihn,
  er will all -- zeit, er will all -- zeit barm -- her -- zig sein,
  er will all -- zeit, er will all -- zeit barm -- her -- zig sein.
}
wordsIISI = \lyricmode {
  \set stanza = "2. "
  Was blei -- ben im -- mer wir da -- heim?
  Lasst uns auch aufs Ge -- bir -- ge gehn,
  da eins dem an -- dern spre -- che zu,
  des Gei -- stes Gruß das Herz auf -- tu,
  da -- von es freu -- dig werd und spring,
  der Mund in wah -- rem Glau -- ben __ sing:
}
wordsISII = \lyricmode {
  \set stanza = "1. "
  Ü -- bers Ge -- birg __ Ma -- ri -- a geht
  zu ih -- rer Bas E -- li -- sa -- beth, E -- li -- sa -- beth.
  Sie grüßt die Freun -- din, die vom Geist
  freu -- dig be -- wegt Ma -- ri -- a preist, Ma -- ri -- a preist.
  Ma -- ri -- a ward fröh -- lich und sang:
  %% Refrain
  Mein Seel den Herrn er -- he -- bet, mein Geist sich Got -- tes freu -- et;
  er ist mein Hei -- land, fürch -- tet ihn,
  er will all -- zeit barm -- her -- zig sein, __
  all -- zeit barm -- her -- zig sein.
}
wordsIISII = \lyricmode {
  \set stanza = "2. "
  Was blei -- ben im -- mer wir da -- heim?
  Lasst uns auch aufs Ge -- bir -- ge, aufs Ge -- bir -- ge gehn,
  da eins dem an -- dern spre -- che zu,
  des Gei -- stes Gruß das Herz auf -- tu, das Herz auf -- tu,
  der Mund in wah -- rem Glau -- ben __ sing:
}
wordsIA = \lyricmode {
  \set stanza = "1. "
  Ü -- bers Ge -- birg Ma -- ri -- a geht __
  zu __ ih -- rer Bas E -- li -- sa -- beth.
  Sie grüßt die Freun -- din, die vom Geist
  freu -- dig be -- wegt Ma -- ri -- a preist
  und sie des Her -- ren Mut -- ter nennt:
  Ma -- ri -- a ward fröh -- lich und __ sang:
  %% Refrain
  Mein Geist sich Got -- tes freu -- et;
  er ist mein Hei -- land, fürch -- tet ihn, fürch -- tet ihn,
  er will all -- zeit, er will all -- zeit barm -- her -- zig sein,
  all -- zeit barm -- her -- zig sein.
}
wordsIIA = \lyricmode {
  \set stanza = "2. "
  Was blei -- ben im -- mer wir da -- heim? __
  Lasst __  uns auch aufs Ge -- bir -- ge gehn,
  da eins dem an -- dern spre -- che zu, __
  des __ Gei -- stes Gruß das Herz auf -- tu,
  da -- von es freu -- dig werd und spring,
  der Mund in wah -- rem Glau -- ben sing:
}
wordsIT = \lyricmode {
  \set stanza = "1. "
  Ü -- bers Ge -- birg Ma -- ri -- a geht
  zu ih -- rer Bas E -- li -- sa -- beth.
  Sie grüßt die Freun -- din, die vom Geist
  freu -- dig be -- wegt Ma -- ri -- a preist
  und sie des Her -- ren Mut -- ter nennt:
  Ma -- ri -- a ward fröh -- lich und __ sang:
  %% Refrain
  Mein Seel den Herrn er -- he -- bet, mein Geist sich Got -- tes freu -- et;
  er ist mein Hei -- land, fürch -- tet ihn,
  er will all -- zeit barm -- her -- zig sein,
  er will all -- zeit barm -- her -- zig sein.
}
wordsIIT = \lyricmode {
  \set stanza = "2. "
  Was blei -- ben im -- mer wir da -- heim?
  Lasst uns auch aufs Ge -- bir -- ge gehn,
  da eins dem an -- dern spre -- che zu,
  des Gei -- stes Gruß das Herz auf -- tu,
  da -- von es freu -- dig werd und spring,
  der Mund in wah -- rem Glau -- ben sing:
}
wordsIB = \lyricmode {
  \set stanza = "1. "
  Ü -- bers Ge -- birg Ma -- ri -- a geht
  zu ih -- rer Bas E -- li -- sa -- beth.
  Sie grüßt die Freun -- din, die vom Geist
  freu -- dig be -- wegt Ma -- ri -- a preist
  und sie des Her -- ren Mut -- ter nennt:
  Ma -- ri -- a ward fröh -- lich und __ sang:
  %% Refrain
  Mein Geist sich Got -- tes freu -- et;
  er ist mein Hei -- land, fürch -- tet ihn, fürch -- tet ihn,
  er will all -- zeit barm -- her -- zig __ sein.
}
wordsIIB = \wordsIIT

\new ChoirStaff <<
    \new Staff = "SI" 
    <<
      \new Voice = "SI" { \global \voiceSI }
    >>
    \new Lyrics \lyricsto "SI" { \wordsISI }
    \new Lyrics \lyricsto "SI" { \wordsIISI }

    \new Staff = "SII"
    <<
      \new Voice = "SII" { \global \voiceSII }
    >>
    \new Lyrics \lyricsto "SII" { \wordsISII }
    \new Lyrics \lyricsto "SII" { \wordsIISII }

    \new Staff = "AI" 
    <<
      \new Voice = "A" { \global \voiceA }
    >>
    \new Lyrics \lyricsto "A" { \wordsIA }
    \new Lyrics \lyricsto "A" { \wordsIIA }

    \new Staff = "T" 
    <<
      \new Voice = "T" { \global \voiceT }
    >>
    \new Lyrics \lyricsto "T" { \wordsIT }
    \new Lyrics \lyricsto "T" { \wordsIIT }

    \new Staff = "B" 
    <<
      \new Voice = "B" { \global \voiceB }
    >>
    \new Lyrics \lyricsto "B" { \wordsIB }
    \new Lyrics \lyricsto "B" { \wordsIIB }
>>  % end ChoirStaff