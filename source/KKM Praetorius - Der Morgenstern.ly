\version "2.20.0"
\language "deutsch"

logo = \markup {
  \with-color #(x11-color 'grey80)
  \filled-box #'(0 . 3) #'(-0.6 . 2.4) #0.2
  \hspace #-3.5
  \with-color #(x11-color 'orange)
  \filled-box #'(1 . 2) #'(0.4 . 1.4) #0.1
  \hspace #1.5
}
    
\header {
  title = "Der Morgenstern ist aufgedrungen"
  subtitle = "EG 69"
  poet = "Text: Daniel Rumpius 1587"
  composer = "Melodie und Satz: Michael Praetorius 1609"
  tagline = \markup { \typewriter https://gitlab.com/Melpomene/scores }
  copyright = \markup {
    \logo
    Konkordien-Kantorei Mannheim, 2020, 
    %% ( \typewriter { https://creativecommons.org/publicdomain/zero/1.0/deed.de} )
    \sans \bold \circle { CC } \sans \bold \circle { "  0  " }
    — Vervielfältigung erwünscht
  }
}

global = { 
  \key g \major
  \override Staff.TimeSignature.style = #'single-digit
  \time 2/2
}


sopranoWordsI = \lyricmode {
  Der Mor -- gen  stern ist auf -- ge -- drun -- gen,
  er leucht da -- her zu die -- ser Stun -- de
  hoch ü -- ber Berg und tie -- fe Tal, __
  vor Freud singt uns der lie -- ben En -- gel Schar.
}
sopranoWordsII = \lyricmode {
  „Wacht auf“, singt uns der Wäch -- ter Stim -- me
  vor Freu -- den auf der ho -- hen Zin -- ne:
  „Wacht auf zu die -- ser Freu -- den -- zeit!“ __
  Der Bräut -- gam kommt, nun ma -- chet euch be -- reit.
}
sopranoWordsIII = \lyricmode {
  O heil -- ger Mor -- gen -- stern, __ wir prei -- sen
  dich heu -- te hoch mit fro -- hen Wei -- sen;
  du leuch -- test -- vie -- len nah __ und fern, __
  so leucht auch uns, Herr Christ, __ du Mor -- gen -- stern!
}

altoWordsI = \sopranoWordsI
altoWordsII = \sopranoWordsII
altoWordsIII = \sopranoWordsIII

tenorWordsI = \lyricmode {
  Der Mor -- gen  stern ist auf -- ge -- drun -- gen,
  er leucht da -- her zu die -- ser Stun -- de
  hoch ü -- ber Berg und tie -- fe Tal,
  vor Freud singt uns der lie -- ben En -- gel Schar.
}
tenorWordsII = \lyricmode {
  „Wacht auf“, singt uns der Wäch -- ter Stim -- me
  vor Freu -- den auf der ho -- hen Zin -- ne:
  „Wacht auf zu die -- ser Freu -- den -- zeit!“
  Der Bräut -- gam kommt, nun ma -- chet euch be -- reit.
}
tenorWordsIII = \lyricmode {
  O heil -- ger Mor -- gen -- stern, __ wir prei -- sen
  dich heu -- te hoch mit fro -- hen Wei -- sen;
  du leuch -- test -- vie -- len nah __ und fern,
  so leucht auch uns, Herr Christ, __ du Mor -- gen -- stern!
}

bassWordsI = \sopranoWordsI
bassWordsII = \sopranoWordsII
bassWordsIII = \sopranoWordsIII


sopranoMusic = \relative c'' {
  \clef "treble"
  \partial 4
  d4 | d h g a | h8( c d2) h4 | c2 h |
  r4 d d2 | h4 a2 g4 | c4 a d2 | h2
  \autoBeamOff
  r4 h4 | h8 h h c d4 d | \autoBeamOn d( c8 h a2 | g4 fis8 e d2) |
  r4 d g g | a a h8 (c d4~ | d) h h( a8 g | h2) a | g1 \bar "|."
}

altoMusic = \relative c'' {
  \clef "treble"
  \partial 4
  g4 | a d, e fis | d4.( e8 fis4) d | a'2 g |
  r4 g fis2 | g4 e2 4 | g fis fis2 | d 
  \autoBeamOff
  r4 d | g8 g g g fis2~ | \autoBeamOn fis fis | e( fis) |
  r4 g d e | e fis d8( e fis4~ | fis) g2( fis8 e) | fis4( g) fis2 | d1
}

tenorMusic = \relative c' {
  \clef "G_8"
  \partial 4
  d4 | d4. h8 h4 d | h( a8 g a4) h | e2 e |
  r4 d4 a2 | e'4 c2 h4 | e d a2 | h2
  \autoBeamOff
  r4 h4 | d8 d d e a,2~ | \autoBeamOn a d | h1 |
  r4 h4 h2 | c4 d h a4~ | a e'4 e2 | d1 | h
}

bassMusic = \relative c' {
  \clef "bass"
  \partial 4
  g4 | fis g e d | g( fis8 e d4) g | a2 e |
  r4 h4 d2 | e4 a,2 e'4 | c d d2 | g,
  \autoBeamOff
  r4 g' | g8 g g e d2~ |  \autoBeamOn d d | e4( d8 c h2) |
  r4 g' g e | a d, g( fis8 e | d4 e2) c4 | d2 d | g,1
}


\new ChoirStaff <<
  \new Staff = "sopranos"
  <<
    \new Voice = "sopranos" {
      \global
      \sopranoMusic
    }
  >>
  \new Lyrics \lyricsto "sopranos" {
    \sopranoWordsI
  }
  \new Lyrics \lyricsto "sopranos" {
    \sopranoWordsII
  }
  \new Lyrics \lyricsto "sopranos" {
    \sopranoWordsIII
  }
  \new Staff = "altos"
  <<
    \new Voice = "altos" {
      \global
      \altoMusic
    }
  >>
  \new Lyrics \lyricsto "altos" {
    \altoWordsI
  }
  \new Lyrics \lyricsto "altos" {
    \altoWordsII
  }
  \new Lyrics \lyricsto "altos" {
    \altoWordsIII
  }
  \new Staff = "tenors"
  <<
    \new Voice = "tenors" {
      \global
      \tenorMusic
    }
  >>
  \new Lyrics \lyricsto "tenors" {
    \tenorWordsI
  }
  \new Lyrics \lyricsto "tenors" {
    \tenorWordsII
  }
  \new Lyrics \lyricsto "tenors" {
    \tenorWordsIII
  }
  \new Staff = "basses"
  <<
    \new Voice = "basses" {
      \global
      \bassMusic
    }
  >>
  \new Lyrics \lyricsto "basses" {
    \bassWordsI
  }
  \new Lyrics \lyricsto "basses" {
    \bassWordsII
  }
  \new Lyrics \lyricsto "basses" {
    \bassWordsIII
  }
>>  % end ChoirStaff