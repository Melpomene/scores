\version "2.24.3"
\language "deutsch"

#(set-default-paper-size "a4")
#(set-global-staff-size 16)

\paper {
%  indent = 0\cm
}

\header {
  title = "Bruckner – Vexilla regis"
  pdftitle = "Bruckner – Vexilla regis"
  subject = "unterwegs behütet"
  author= "Richard Hirsch"
  copyright = "Konkordien-Kantorei Mannheim 2024 (rev. 2) – Vervielfältigung und Verbreitung erlaubt"
  tagline = ""
}

crescsempre = \markup { \italic "cresc. sempre" }
dimsempre =  \markup { \italic "dim. sempre" }

barFermata = {
  \once \set Staff.caesuraType = #'((underlying-bar-line . "||"))
  \once \set Staff.caesuraTypeTransform = ##f
  \caesura ^\fermata
}

vexilla_meta = {
  \key e \phrygian
  \time 2/2
  \dynamicUp
}

vexilla_one_text = \lyricmode {
  Ve -- xil -- la re -- gis pro -- de -- unt 
  ful -- get cru -- cis, ful -- get cru -- cis my -- ste -- ri -- um
  quo car -- ne car -- nis con -- di -- tor
  su -- spen -- sus, su -- spen -- sus est __ pa -- ti -- bu -- lo.
}

vexilla_two_text_s = \lyricmode {
  O __ crux __ a -- ve __ spes u -- ni -- ca
  hoc pas -- si -- o -- nis, pa -- si -- o -- nis tem -- po -- re
  au -- ge pi -- is ju -- sti -- ti -- am
  re -- i -- sque do -- na, do -- na, do -- na ve -- ni -- am.
}

vexilla_two_text_a = \lyricmode {
  O __ crux a -- ve spes u -- ni -- ca
  hoc pas -- si -- o -- nis, pa -- si -- o -- nis tem -- po -- re
  au -- ge pi -- is ju -- sti -- ti -- am
  re -- i -- sque do -- na, do -- na ve -- ni -- am.
}

vexilla_two_text_t = \lyricmode {
  O crux __ a -- ve spes u -- ni -- ca
  hoc pas -- si -- o -- nis, pa -- si -- o -- nis tem -- po -- re
  au -- ge pi -- is ju -- sti -- ti -- am
  re -- i -- sque do -- na, __ do -- na ve -- ni -- am.
}

vexilla_two_text_b = \lyricmode {
  O crux __ a -- ve spes u -- ni -- ca
  hoc pas -- si -- o -- nis, pa -- si -- o -- nis tem -- po -- re
  au -- ge pi -- is ju -- sti -- ti -- am
  re -- i -- sque do -- na, do -- na ve -- ni -- am.
}

vexilla_three_text = \lyricmode {
  Te sum -- ma -- De -- us __ Tri -- ni -- tas
  col -- lau -- det, col -- lau -- det om -- nis spi -- ri -- tus
  quos __ per cru -- cis my -- ste -- ri -- um
  sal -- vas re -- ge per sae -- cu -- la.
  A -- men, A -- men.
}

\include "Bruckner - Vexilla regis/soprano.ly"
\include "Bruckner - Vexilla regis/alto.ly"
\include "Bruckner - Vexilla regis/tenore.ly"
\include "Bruckner - Vexilla regis/basso.ly"


%%% Vexilla regus – stanza 1
\score {
  \header { piece = "Vexilla regis – Strophe 1" }
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Sopran " }
    <<
      \new Voice = "soprano" { 
        \vexilla_meta
        \vexilla_one_soprano
      }
      \new Lyrics \lyricsto "soprano" {
        \vexilla_one_text
      }
    >>
    \new Staff \with { instrumentName = "Alt " }
    <<
      \new Voice = "alto" { 
        \vexilla_meta
        \vexilla_one_alto
      }
      \new Lyrics \lyricsto "alto" {
        \vexilla_one_text
      }
    >>
    \new Staff \with { instrumentName = "Tenor " }
    << 
      \new Voice = "tenore" { 
        \vexilla_meta
        \clef "G_8"
        \vexilla_one_tenore
      }
      \new Lyrics \lyricsto "tenore" {
        \vexilla_one_text
      }
    >>
    \new Staff \with { instrumentName = "Bass " }
    <<
      \new Voice = "basso" { 
        \vexilla_meta
        \clef "bass"
        \vexilla_one_basso
      }
      \new Lyrics \lyricsto "basso" {
	Ve -- xil -- la re -- gis pro -- de -- unt 
	ful -- get cru -- cis, ful -- get cru -- cis my -- ste -- ri -- um
	quo car -- ne car -- nis con -- di -- tor
	su -- spen -- sus, su -- spen -- sus est pa -- ti -- bu -- lo.
      }
    >>
  >>
}

%%% Vexilla regis – stanza 2
\score {
  \header { piece = "Vexilla regis – Strophe 2" }
  \new ChoirStaff <<
    \new Staff = "soprano" <<
      \new Voice = "soprano" { 
        \vexilla_meta
        \vexilla_two_soprano
      }
      \new Lyrics \lyricsto "soprano" {
        \vexilla_two_text_s
      }
    >>
    \new Staff = "alto" <<
      \new Voice = "alto" { 
        \vexilla_meta
        \vexilla_two_alto
      }
      \new Lyrics \lyricsto "alto" {
        \vexilla_two_text_a
      }
    >>
    \new Staff = "tenore" <<
      \new Voice = "tenore" { 
        \vexilla_meta
        \clef "G_8"
        \vexilla_two_tenore
      }
      \new Lyrics \lyricsto "tenore" {
        \vexilla_two_text_t
      }
    >>
    \new Staff = "basso" <<
      \new Voice = "basso" { 
        \vexilla_meta
        \clef "bass"
        \vexilla_two_basso
      }
      \new Lyrics \lyricsto "basso" {
        \vexilla_two_text_b
      }
    >>
  >>
}

%%% Vexilla regis – stanza 3
\score {
  \header { piece = "Vexilla regis – Strophe 3" }
  \new ChoirStaff <<
    \new Staff = "soprano" <<
      \new Voice = "soprano" { 
        \vexilla_meta
        \vexilla_three_soprano
      }
      \new Lyrics \lyricsto "soprano" {
        \vexilla_three_text
      }
    >>
    \new Staff = "alto" <<
      \new Voice = "alto" { 
        \vexilla_meta
        \vexilla_three_alto
      }
      \new Lyrics \lyricsto "alto" {
        \vexilla_three_text
      }
    >>
    \new Staff = "tenore" <<
      \new Voice = "tenore" { 
        \vexilla_meta
        \clef "G_8"
        \vexilla_three_tenore
      }
      \new Lyrics \lyricsto "tenore" {
	Te sum -- ma -- De -- us __ Tri -- ni -- tas
	col -- lau -- det, col -- lau -- det om -- nis spi -- ri -- tus
	quos __ per cru -- cis my -- ste -- ri -- um
	sal -- vas re -- ge per __ sae -- cu -- la.
	A -- men, A -- men.
      }
    >>
    \new Staff = "basso" <<
      \new Voice = "basso" { 
        \vexilla_meta
        \clef "bass"
        \vexilla_three_basso
      }
      \new Lyrics \lyricsto "basso" {
        \vexilla_three_text
      }
    >>
  >>
}
