[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg)](https://lbesson.mit-license.org/)
[![Lilypond 2.20.0](https://img.shields.io/badge/LilyPond-2.20.0-brightgreen)](https://lilypond.org)


# ![Logo](https://static.wixstatic.com/media/d00fb5_b774a5e2e68f46fc8f0de86ddf9bb0a5~mv2.jpg/v1/fill/w_56,h_56,al_c,lg_1,q_80/logo.webp) Noten für die Konkordien-Kantorei Mannheim

## Inhalt

| Komponist | Titel | Werknummer | Dateiname | Lizenz |
| --------- | ----- | ---------- | ----- | ------ |
| Bruckner, Anton | Vexilla regis | WAB 51| `Bruckner_-_Vexilla_regis.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Crüger, Johann | O Haupt voll Blut und Wunden | | `Crüger - O Haupt voll Blut und Wunden.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Crüger, Johann | O Haupt voll Blut und Wunden | | `Crüger - O Haupt voll Blut und Wunden (HKJ).ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Eccard, Johann | Übers Gebirg Maria geht |  | `Eccard - Übers Gebirg.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Kaminski, Heinrich | Maria durch ein Dornenwald ging |  | `Kaminski - Maria.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Mendelssohn, Felix | Frohlocket ihr Völker | MWV B 42 (Op. 79, Nr. 1)| `Mendelssohn - Weihnacht.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Mendelssohn, Felix | Mitten wir im Leben sind | MWV B 21 (Op. 23, Nr. 3)| `Mendelssohn_- Mitten_wir_im_Leben_sind.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Praetorius, Michael | Der Morgenstern ist aufgedrungen | | `Praetorius - Der Morgenstern.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Riedel, Carl | Freu dich, Erd und Sternenzelt | | `Riedel - Freu dich.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Riedel, Carl | Kommet, ihr Hirten | | `Riedel - Kommet ihr Hirten.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |
| Silcher, Friedrich | Schau hin nach Golgatha | op. 9 Nr. 4 | `Silcher - Schau hin nach Golgatha.ly` | [![CC-0 license](https://img.shields.io/badge/License-CC--0-blue.svg)](https://creativecommons.org/licenses/by-nd/4.0)  |


## Lilypond

gesetzt mit [Lilypond](https://lilypond.org)

![Lilypond](http://lilypond.org/pictures/double-lily-modified3.png "Lilypond Logo")

